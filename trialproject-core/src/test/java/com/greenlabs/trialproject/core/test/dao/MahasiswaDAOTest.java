package com.greenlabs.trialproject.core.test.dao;

import com.greenlabs.trialproject.core.dao.MahasiswaDAO;
import com.greenlabs.trialproject.core.entity.Mahasiswa;
import com.greenlabs.trialproject.core.entity.User;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Date;

/**
 * Created by Tyas on 11/24/2014.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:core-context.xml"})
public class MahasiswaDAOTest {
    @Autowired
    private MahasiswaDAO mahasiswaDAO;
    private Mahasiswa mahasiswa;

    @Before
    public void init() {
        mahasiswa = new Mahasiswa();
        mahasiswa.setNIM("0897");
        mahasiswa.setNama("Paijo");
        mahasiswa.setJurusan("TI");
        mahasiswa.setAlamat("Seturan");
        mahasiswa.setCreatedBy(new User());
        mahasiswa.setCreatedTime(new Date());
    }

    @Test
    public void save() {
        mahasiswaDAO.save(mahasiswa);
    }

    @Test
    public void delete() {
        mahasiswaDAO.delete(mahasiswa);
    }


    @Test
    public void update() {
        mahasiswaDAO.update(mahasiswa);
    }


    @Test
    public void find() {
        mahasiswaDAO.find(mahasiswa, 0, Integer.MAX_VALUE);
    }


    @Test
    public void findById() {
        mahasiswaDAO.findById(mahasiswa.getId());
    }


    @Test
    public void count() {
        mahasiswaDAO.count(mahasiswa);
    }
}
