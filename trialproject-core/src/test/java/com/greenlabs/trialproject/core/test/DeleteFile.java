package com.greenlabs.trialproject.core.test;

import org.apache.commons.io.FileUtils;

import java.io.File;

/**
 * Created by krissadewo on 1/29/14.
 */
public class DeleteFile {

    public static void main(String[] args) {
        File file = new File("C:/Users/krissadewo/AppData/Local/Temp/");
        if (file.isDirectory()) {
            for (File c : file.listFiles()) {
                try {
                    FileUtils.forceDelete(c);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        }

    }
}
