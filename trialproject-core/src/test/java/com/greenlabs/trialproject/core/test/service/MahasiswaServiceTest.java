package com.greenlabs.trialproject.core.test.service;

import com.greenlabs.trialproject.core.common.Constant;
import com.greenlabs.trialproject.core.entity.Buku;
import com.greenlabs.trialproject.core.entity.Mahasiswa;
import com.greenlabs.trialproject.core.entity.User;
import com.greenlabs.trialproject.core.service.BukuService;
import com.greenlabs.trialproject.core.service.MahasiswaService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by Tyas on 11/24/2014.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:core-context.xml"})
@WebAppConfiguration
public class MahasiswaServiceTest {
    @Autowired
    private MockHttpSession mockHttpSession;
    @Autowired
    private MahasiswaService mahasiswaService;
    private Mahasiswa mahasiswa;

    @Before
    public void init() {
        mockHttpSession.setAttribute(Constant.SESSION_USER,new User(1l));

        mahasiswa = new Mahasiswa();
        mahasiswa.setNIM("089");
        mahasiswa.setNama("Paijo");
        mahasiswa.setJurusan("TI");
        mahasiswa.setAlamat("Jogja");
        mahasiswa.setCreatedBy((User) mockHttpSession.getAttribute(Constant.SESSION_USER));
    }

    @Test
    @Transactional
    public void save() {
        Assert.assertNotNull(mahasiswaService.save(mahasiswa));

    }

    @Test
    @Transactional
    public void delete() {
        System.out.println(mahasiswaService.delete(mahasiswa));
    }

    @Test
    @Transactional
    public void count() {
        mahasiswaService.count(mahasiswa);
    }

    @Test
    @Transactional
    public void findById() {
        mahasiswaService.findById(mahasiswa.getId());
    }

    @Test
    @Transactional
    public void find() {
        mahasiswaService.find(mahasiswa, 1, 1);
    }
}
