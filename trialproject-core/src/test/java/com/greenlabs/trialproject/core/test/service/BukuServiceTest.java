package com.greenlabs.trialproject.core.test.service;

import com.greenlabs.trialproject.core.common.Constant;
import com.greenlabs.trialproject.core.entity.Buku;
import com.greenlabs.trialproject.core.entity.Role;
import com.greenlabs.trialproject.core.entity.User;
import com.greenlabs.trialproject.core.service.BukuService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

/**
 * Created by Tyas on 11/19/2014.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:core-context.xml"})
@WebAppConfiguration
public class BukuServiceTest {

    @Autowired
    private MockHttpSession mockHttpSession;
    @Autowired
    private BukuService bukuService;
    private Buku buku;

    @Before
    public void init() {
        mockHttpSession.setAttribute(Constant.SESSION_USER,new User(1l));

        buku = new Buku();
        buku.setKode("678");
        buku.setJudul("Kamuuuu");
        buku.setPenulis("Akuuuu");
        buku.setPenerbit("Diaaaa");
        buku.setCreatedBy((User) mockHttpSession.getAttribute(Constant.SESSION_USER));
    }

    @Test
    @Transactional
    public void save() {
        Assert.assertNotNull(bukuService.save(buku));

    }

    @Test
    @Transactional
    public void delete() {
        System.out.println(bukuService.delete(buku));
    }

    @Test
    @Transactional
    public void count() {
        bukuService.count(buku);
    }

    @Test
    @Transactional
    public void findById() {
        bukuService.findById(buku.getId());
    }

    @Test
    @Transactional
    public void find() {
        bukuService.find(buku, 0, Integer.MAX_VALUE);
    }

}
