/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.greenlabs.trialproject.core.test.dao;

import com.greenlabs.trialproject.core.AppCore;
import com.greenlabs.trialproject.core.dao.BukuDAO;
import com.greenlabs.trialproject.core.dao.UserDAO;
import com.greenlabs.trialproject.core.entity.Buku;
import com.greenlabs.trialproject.core.entity.Role;
import com.greenlabs.trialproject.core.entity.User;
import org.joda.time.DateTime;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * @author krissadewo
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:core-context.xml"})
public class BukuDAOTest {

    @Autowired
    private BukuDAO bukuDAO;
    private Buku buku;


    @Before
    public void init() {
        User user = new User("Paijo", "paijo");
        user.setId(98L);
        user.setDelete(false);
        user.setRole(new Role());
        user.setSession("ora ngerti");

        buku = new Buku();
        buku.setKode("678");
        buku.setJudul("Kamuuuu");
        buku.setPenulis("Akuuuu");
        buku.setPenerbit("Diaaaa");
        buku.setCreatedBy(user);

    }

    @Test
//    @Transactional
    public void delete() {
//        AppCore.getLogger(this).info("OK", bukuDAO.delete(buku));
        Assert.assertNotNull(bukuDAO.delete(buku));
    }

    @Test
    //@Transactional
    public void save() {
        bukuDAO.save(buku);
//        Assert.assertNotNull(bukuDAO.save(buku));
//        Assert.assertEquals(buku, bukuDAO.save(buku));
//        Assert.assertNotNull(bukuDAO.save(buku));
    }

    @Test
    @Transactional
    public void update() {
        bukuDAO.update(buku);
    }

    @Test
    public void find() {
        List<Buku> bukus = bukuDAO.find(new Buku(), 0, Integer.MAX_VALUE);
        Assert.assertEquals(1, bukus.get(0));
    }

    @Test
    public void getSize() {
        bukuDAO.count(buku);
        System.out.println("" + bukuDAO.count(buku));
    }


}
