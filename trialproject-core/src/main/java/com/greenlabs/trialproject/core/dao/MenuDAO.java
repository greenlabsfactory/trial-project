/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.greenlabs.trialproject.core.dao;

import com.greenlabs.trialproject.core.entity.Menu;
import com.greenlabs.trialproject.core.entity.Role;
import com.greenlabs.trialproject.core.entity.User;

import java.util.List;

/**
 * @author krissadewo
 */
public interface MenuDAO extends BaseDAO<Menu> {

    /**
     * Get menu by user
     *
     * @param user user
     * @return menus
     */
    List<Menu> find(User user);

    /**
     * Just little bit different with find with parameter user, this used
     * for hak akses view
     *
     * @param role role
     * @return menus
     */
    List<Menu> find(Role role);
}
