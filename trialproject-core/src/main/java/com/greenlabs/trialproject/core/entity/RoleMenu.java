/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.greenlabs.trialproject.core.entity;

import java.io.Serializable;

/**
 * @author krissadewo
 */
public class RoleMenu implements Serializable {

    private static final long serialVersionUID = -5730783828158522363L;
    private Long id;
    private Menu menu;
    private Role role;
    private boolean canEdit;
    private boolean canRead;
    private boolean canSave;
    private boolean canDelete;
    private boolean canPrint;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Menu getMenu() {
        return menu;
    }

    public void setMenu(Menu menu) {
        this.menu = menu;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public boolean isCanEdit() {
        return canEdit;
    }

    public void setCanEdit(boolean canEdit) {
        this.canEdit = canEdit;
    }

    public boolean isCanSave() {
        return canSave;
    }

    public void setCanSave(boolean canSave) {
        this.canSave = canSave;
    }

    public boolean isCanDelete() {
        return canDelete;
    }

    public void setCanDelete(boolean canDelete) {
        this.canDelete = canDelete;
    }

    public boolean isCanRead() {
        return canRead;
    }

    public void setCanRead(boolean canRead) {
        this.canRead = canRead;
    }

    public boolean isCanPrint() {
        return canPrint;
    }

    public void setCanPrint(boolean canPrint) {
        this.canPrint = canPrint;
    }

    @Override
    public String toString() {
        return "RoleMenu{" +
                "id=" + id +
                ", menu=" + menu +
                ", role=" + role +
                ", canEdit=" + canEdit +
                ", canRead=" + canRead +
                ", canSave=" + canSave +
                ", canDelete=" + canDelete +
                ", canPrint=" + canPrint +
                '}';
    }
}
