/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.greenlabs.trialproject.core.dao;

import com.greenlabs.trialproject.core.entity.RoleMenu;

/**
 * @author krissadewo <dailycode.org>
 * @date Jul 25, 2013
 */
public interface RoleMenuDAO extends BaseDAO<RoleMenu> {
}
