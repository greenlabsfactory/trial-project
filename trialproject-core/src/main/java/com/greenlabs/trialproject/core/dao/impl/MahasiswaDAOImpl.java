package com.greenlabs.trialproject.core.dao.impl;

import com.greenlabs.trialproject.core.dao.MahasiswaDAO;
import com.greenlabs.trialproject.core.entity.Buku;
import com.greenlabs.trialproject.core.entity.Mahasiswa;
import com.greenlabs.trialproject.core.entity.User;
import com.greenlabs.trialproject.core.util.Table;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Tyas on 11/24/2014.
 */
@Repository
public class MahasiswaDAOImpl implements MahasiswaDAO {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public Mahasiswa save(Mahasiswa entity) {
        String sql = "INSERT INTO " + Table.MASTER_MAHASISWA + " (" +
                "nim, " +
                "nama," +
                "jurusan," +
                "alamat," +
                "created_by," +
                "created_time) " +
                "VALUES(?,?,?,?,?,?) ";

        jdbcTemplate.update(sql,
                entity.getNIM(),
                entity.getNama(),
                entity.getJurusan(),
                entity.getAlamat(),
                entity.getCreatedBy().getId(),
                new Timestamp(entity.getCreatedTime().getTime()));
        return entity;
    }

    @Override
    public Mahasiswa update(Mahasiswa entity) {
        String sql = "UPDATE " + Table.MASTER_MAHASISWA + " SET " +
                "nim = ?, " +
                "nama = ?, " +
                "jurusan = ?, " +
                "alamat = ? " +
                "WHERE id =  ? ";

        jdbcTemplate.update(sql,
                entity.getNIM(),
                entity.getNama(),
                entity.getJurusan(),
                entity.getAlamat(),
                entity.getId());
        return entity;
    }

    @Override
    public Mahasiswa delete(Mahasiswa entity) {
        String sql = "DELETE FROM " + Table.MASTER_MAHASISWA+ " WHERE id =  ?";

        jdbcTemplate.update(sql, entity.getId());
        return entity;
    }

    @Override
    public Mahasiswa findById(Long id) {
        String sql = "SELECT * FROM " + Table.MASTER_MAHASISWA+ " a " +
                "INNER JOIN " + Table.SYSTEM_USER + " u ON u.id = a.created_by " +
                "WHERE 1 = 1 " +
                "AND a.id = ? ";

        try {
            return jdbcTemplate.queryForObject(sql, new MahasiswaRowMapper(), id);
        } catch (EmptyResultDataAccessException ignored) {
        }
        return null;
    }

    @Override
    public List<Mahasiswa> find(Mahasiswa param, Integer offset, Integer limit) {
        String sql = "SELECT * FROM " + Table.MASTER_MAHASISWA + " a " +
                "INNER JOIN " + Table.SYSTEM_USER + " u ON u.id = a.created_by " +
                "WHERE 1 = 1 ";

        List<Object> params = new ArrayList<>();

        if (param.getNIM() != null) {
            params.add("%" + param.getNIM() + "%");
            sql += "AND nim LIKE ? ";
        }
        return jdbcTemplate.query(sql, params.toArray(), new MahasiswaRowMapper());
    }

    @Override
    public int count(Mahasiswa param) {
        String sql = "SELECT COUNT(id) FROM " + Table.MASTER_MAHASISWA + " WHERE 1 = 1 ";

        List<Object> params = new ArrayList<>();

        if (param.getNIM() != null) {
            params.add("%" + param.getNIM() + "%");
            sql += " AND nim LIKE ? ";
        }

        return jdbcTemplate.queryForObject(sql, params.toArray(), Integer.class);
    }

    class MahasiswaRowMapper implements RowMapper<Mahasiswa> {
        @Override
        public Mahasiswa mapRow(ResultSet rs, int i) throws SQLException {
            User user = new User();
            user.setRealname(rs.getString("realname"));

            Mahasiswa mahasiswa = new Mahasiswa();
            mahasiswa.setId(rs.getLong("id"));
            mahasiswa.setNIM(rs.getString("nim"));
            mahasiswa.setNama(rs.getString("nama"));
            mahasiswa.setJurusan(rs.getString("jurusan"));
            mahasiswa.setAlamat(rs.getString("alamat"));
            mahasiswa.setCreatedBy(user);
            mahasiswa.setCreatedTime(rs.getTimestamp("created_time"));
            return mahasiswa;
        }
    }
}
