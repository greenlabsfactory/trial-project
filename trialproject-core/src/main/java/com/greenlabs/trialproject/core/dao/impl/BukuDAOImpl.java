package com.greenlabs.trialproject.core.dao.impl;

import com.greenlabs.trialproject.core.dao.BukuDAO;
import com.greenlabs.trialproject.core.entity.Buku;
import com.greenlabs.trialproject.core.entity.User;
import com.greenlabs.trialproject.core.util.Table;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Tyas on 11/19/2014.
 */
@Repository
public class BukuDAOImpl implements BukuDAO {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public Buku save(Buku entity) {
        String sql = "INSERT INTO " + Table.MASTER_BUKU + " (" +
                "kode," +
                "judul," +
                "penulis," +
                "penerbit," +
                "created_by," +
                "created_time) " +
                "VALUES(?,?,?,?,?,?) ";

        jdbcTemplate.update(sql,
                entity.getKode(),
                entity.getJudul(),
                entity.getPenulis(),
                entity.getPenerbit(),
                entity.getCreatedBy().getId(),
                new Timestamp(entity.getCreatedTime().getTime()));
        return entity;
    }

    @Override
    public Buku update(Buku entity) {
        String sql = "UPDATE " + Table.MASTER_BUKU + " SET " +
                "kode = ?, " +
                "judul = ?, " +
                "penulis = ?, " +
                "penerbit = ? " +
                "WHERE id =  ? ";

        jdbcTemplate.update(sql,
                entity.getKode(),
                entity.getJudul(),
                entity.getPenulis(),
                entity.getPenerbit(),
                entity.getId());
        return entity;
    }

    @Override
    public Buku delete(Buku entity) {
        String sql = "DELETE FROM " + Table.MASTER_BUKU + " WHERE id =  ?";

        jdbcTemplate.update(sql, entity.getId());
        return entity;
    }

    @Override
    public Buku findById(Long id) {
        String sql = "SELECT * FROM " + Table.MASTER_BUKU + " a " +
                "INNER JOIN " + Table.SYSTEM_USER + " u ON u.id = a.created_by " +
                "WHERE 1 = 1 " +
                "AND a.id = ? ";

        try {
            return jdbcTemplate.queryForObject(sql, new BukuRowMapper(), id);
        } catch (EmptyResultDataAccessException ignored) {
        }
        return null;
    }

    @Override
    public List<Buku> find(Buku param, Integer offset, Integer limit) {
        String sql = "SELECT * FROM " + Table.MASTER_BUKU + " a " +
                "INNER JOIN " + Table.SYSTEM_USER + " u ON u.id = a.created_by " +
                "WHERE 1 = 1 ";

        List<Object> params = new ArrayList<>();

        if (param.getKode() != null) {
            params.add("%" + param.getKode() + "%");
            sql += "AND kode LIKE ? ";
        }
        return jdbcTemplate.query(sql, params.toArray(), new BukuRowMapper());
    }

    @Override
    public int count(Buku param) {
        String sql = "SELECT COUNT(id) FROM " + Table.MASTER_BUKU + " WHERE 1 = 1 ";

        List<Object> params = new ArrayList<>();

        if (param.getKode() != null) {
            params.add("%" + param.getKode() + "%");
            sql += " AND kode LIKE ? ";
        }

        return jdbcTemplate.queryForObject(sql, params.toArray(), Integer.class);
    }

    class BukuRowMapper implements RowMapper<Buku> {
        @Override
        public Buku mapRow(ResultSet rs, int i) throws SQLException {
            User user = new User();
            user.setRealname(rs.getString("realname"));

            Buku buku = new Buku();
            buku.setId(rs.getLong("id"));
            buku.setKode(rs.getString("kode"));
            buku.setJudul(rs.getString("judul"));
            buku.setPenulis(rs.getString("penulis"));
            buku.setPenerbit(rs.getString("penerbit"));
            buku.setCreatedBy(user);
            buku.setCreatedTime(rs.getTimestamp("created_time"));
            return buku;
        }
    }
}
