package com.greenlabs.trialproject.core.entity;

import java.io.Serializable;

/**
 * Created by kris on 14/05/14.
 */
public class Agama extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 2739848234600166610L;
    private String nama;

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Agama agama = (Agama) o;

        if (nama != null ? !nama.equals(agama.nama) : agama.nama != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return nama != null ? nama.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "Agama{" +
                "nama='" + nama + '\'' +
                '}';
    }
}
