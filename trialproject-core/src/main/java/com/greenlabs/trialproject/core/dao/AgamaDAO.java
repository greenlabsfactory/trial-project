package com.greenlabs.trialproject.core.dao;

import com.greenlabs.trialproject.core.entity.Agama;

/**
 * Created by kris on 14/05/14.
 */
public interface AgamaDAO extends BaseDAO<Agama> {
}
