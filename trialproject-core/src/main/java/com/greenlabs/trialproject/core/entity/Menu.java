/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.greenlabs.trialproject.core.entity;

import java.io.Serializable;

/**
 * @author kris
 */
public class Menu implements Serializable {

    private static final long serialVersionUID = -812474954969519125L;
    private Long id;
    private String nama;
    private String title;
    private Long idParent;
    private Integer orderNumber;
    private String kode;
    //Hanya untuk penanda saja yang digunakan pada saat edit user menu
    private Role role;
    private RoleMenu roleMenu = new RoleMenu();

    public Menu() {
    }

    public Menu(Long id) {
        this.id = id;
    }

    public Menu(Long id, String nama) {
        this.id = id;
        this.nama = nama;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public Long getIdParent() {
        return idParent;
    }

    public void setIdParent(Long idParent) {
        this.idParent = idParent;
    }

    public Integer getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(Integer orderNumber) {
        this.orderNumber = orderNumber;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public RoleMenu getRoleMenu() {
        return roleMenu;
    }

    public void setRoleMenu(RoleMenu roleMenu) {
        this.roleMenu = roleMenu;
    }

    public String getKode() {
        return kode;
    }

    public void setKode(String kode) {
        this.kode = kode;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        return "Menu{" +
                "id=" + id +
                ", nama='" + nama + '\'' +
                ", title='" + title + '\'' +
                ", idParent=" + idParent +
                ", orderNumber=" + orderNumber +
                ", kode='" + kode + '\'' +
                ", role=" + role +
                ", roleMenu=" + roleMenu +
                '}';
    }
}
