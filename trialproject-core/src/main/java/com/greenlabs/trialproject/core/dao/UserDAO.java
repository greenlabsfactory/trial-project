/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.greenlabs.trialproject.core.dao;


import com.greenlabs.trialproject.core.entity.User;

/**
 * @author krissadewo
 */
public interface UserDAO extends BaseDAO<User> {

    User getByUsername(String username);

    String getPassword(User user);

    User getLogUser(String session);

    User getLogUser(Long id);

    User saveLogin(User entity);

    User saveLogout(User entity);
}
