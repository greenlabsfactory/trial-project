package com.greenlabs.trialproject.core.dao;

import com.greenlabs.trialproject.core.entity.RoleUser;
import com.greenlabs.trialproject.core.entity.User;

import java.util.List;

/**
 * @author krissadewo
 *         5/6/2014,10:27 AM
 */
public interface RoleUserDAO extends BaseDAO<RoleUser> {

    List<RoleUser> save(List<RoleUser> roleUsers);

    List<RoleUser> find(User user);

    /**
     * Delete role user by id users
     *
     * @param roleUsers list of role user
     */
    void delete(List<RoleUser> roleUsers);
}
