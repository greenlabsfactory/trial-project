package com.greenlabs.trialproject.core.util;

/**
 * Created by kris on 15/05/14.
 * Provide utility for DML,DDL
 */
public class Table {

    /**
     * All table name need separate with space at the end of sentence
     */
    public static final String MASTER_MAHASISWA = "master_mahasiswa ";
    public static final String MASTER_BUKU = "master_buku ";
    public static final String MASTER_AGAMA = "master_agama ";
    public static final String TRANSAKSI_PEMINJAMAN = "master_peminjaman ";

    public static final String SYSTEM_MENU = "system_menu ";
    public static final String SYSTEM_ROLE = "system_role ";
    public static final String SYSTEM_USER = "system_user ";
    public static final String SYSTEM_ROLE_USER = "system_role_user ";
    public static final String SYSTEM_ROLE_MENU = "system_role_menu ";

    public static final String LOG_USER = "log_user ";


}
