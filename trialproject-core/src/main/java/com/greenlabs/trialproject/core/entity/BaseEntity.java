package com.greenlabs.trialproject.core.entity;


import java.io.Serializable;
import java.util.Date;

/**
 * Created by kris on 14/05/14.
 * base entity class providing standard property to use in persistent media
 */
public class BaseEntity implements Serializable{

    private static final long serialVersionUID = -8528633195887266795L;
    private Long id;
    private User createdBy;
    private Date createdTime = new Date();
    private User deletedBy;
    private Date deletedTime = new Date();
    //for custom searching from end user
    private String searchValue;
    private String searcKey;
    private Date searchStartDate;
    private Date searchEndDate;

    public BaseEntity() {
    }

    public BaseEntity(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(User createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    public User getDeletedBy() {
        return deletedBy;
    }

    public void setDeletedBy(User deletedBy) {
        this.deletedBy = deletedBy;
    }

    public Date getDeletedTime() {
        return deletedTime;
    }

    public void setDeletedTime(Date deletedTime) {
        this.deletedTime = deletedTime;
    }

    public String getSearchValue() {
        return searchValue;
    }

    public void setSearchValue(String searchValue) {
        this.searchValue = searchValue;
    }

    public String getSearcKey() {
        return searcKey;
    }

    public void setSearcKey(String searcKey) {
        this.searcKey = searcKey;
    }

    public Date getSearchStartDate() {
        return searchStartDate;
    }

    public void setSearchStartDate(Date searchStartDate) {
        this.searchStartDate = searchStartDate;
    }

    public Date getSearchEndDate() {
        return searchEndDate;
    }

    public void setSearchEndDate(Date searchEndDate) {
        this.searchEndDate = searchEndDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof BaseEntity)) return false;

        BaseEntity that = (BaseEntity) o;

        if (createdBy != null ? !createdBy.equals(that.createdBy) : that.createdBy != null) return false;
        if (createdTime != null ? !createdTime.equals(that.createdTime) : that.createdTime != null) return false;
        if (deletedBy != null ? !deletedBy.equals(that.deletedBy) : that.deletedBy != null) return false;
        if (deletedTime != null ? !deletedTime.equals(that.deletedTime) : that.deletedTime != null) return false;
        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (searcKey != null ? !searcKey.equals(that.searcKey) : that.searcKey != null) return false;
        if (searchEndDate != null ? !searchEndDate.equals(that.searchEndDate) : that.searchEndDate != null)
            return false;
        if (searchStartDate != null ? !searchStartDate.equals(that.searchStartDate) : that.searchStartDate != null)
            return false;
        if (searchValue != null ? !searchValue.equals(that.searchValue) : that.searchValue != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (createdBy != null ? createdBy.hashCode() : 0);
        result = 31 * result + (createdTime != null ? createdTime.hashCode() : 0);
        result = 31 * result + (deletedBy != null ? deletedBy.hashCode() : 0);
        result = 31 * result + (deletedTime != null ? deletedTime.hashCode() : 0);
        result = 31 * result + (searchValue != null ? searchValue.hashCode() : 0);
        result = 31 * result + (searcKey != null ? searcKey.hashCode() : 0);
        result = 31 * result + (searchStartDate != null ? searchStartDate.hashCode() : 0);
        result = 31 * result + (searchEndDate != null ? searchEndDate.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "BaseEntity{" +
                "id=" + id +
                ", createdBy=" + createdBy +
                ", deletedBy=" + deletedBy +
                ", searchValue='" + searchValue + '\'' +
                ", searcKey='" + searcKey + '\'' +
                ", searchStartDate=" + searchStartDate +
                ", searchEndDate=" + searchEndDate +
                '}';
    }
}
