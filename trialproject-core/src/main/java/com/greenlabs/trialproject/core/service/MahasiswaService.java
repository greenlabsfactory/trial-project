package com.greenlabs.trialproject.core.service;

import com.greenlabs.trialproject.core.AppCore;
import com.greenlabs.trialproject.core.common.Constant;
import com.greenlabs.trialproject.core.common.Result;
import com.greenlabs.trialproject.core.dao.MahasiswaDAO;
import com.greenlabs.trialproject.core.entity.Buku;
import com.greenlabs.trialproject.core.entity.Mahasiswa;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;

import java.util.List;

/**
 * Created by Tyas on 11/24/2014.
 */
@Service
public class MahasiswaService extends BaseService {
    @Autowired
    public MahasiswaDAO mahasiswaDAO;

    public Result save(final Mahasiswa mahasiswa) {
        return transactionTemplate.execute(new TransactionCallback<Result>() {
            @Override
            public Result doInTransaction(TransactionStatus status) {
                try {
                    mahasiswa.setCreatedBy(AppCore.getInstance().getUserFromSession());

                    if (mahasiswa.getId() == null) {
                        mahasiswaDAO.save(mahasiswa);
                    } else {
                        mahasiswaDAO.update(mahasiswa);
                    }
                } catch (DataAccessException e) {
                    status.setRollbackOnly();
                    throw e;
                }

                return null;
            }
        });
    }

    public Result delete(final Mahasiswa mahasiswa) {
        return transactionTemplate.execute(new TransactionCallback<Result>() {
            @Override
            public Result doInTransaction(TransactionStatus status) {
                try {
                    mahasiswa.setNIM(mahasiswa.getNIM().concat(Constant.FLAG_DELETE));
                    mahasiswa.setDeletedBy(AppCore.getInstance().getUserFromSession());
                    mahasiswaDAO.delete(mahasiswa);
                } catch (DataAccessException e) {
                    status.setRollbackOnly();
                    throw e;
                }

                return null;
            }
        });
    }

    public Mahasiswa findById(Long id) {
        return mahasiswaDAO.findById(id);
    }

    public List<Mahasiswa> find(Mahasiswa mahasiswa, int offset, int limit) {
        return mahasiswaDAO.find(mahasiswa, offset, limit);
    }

    public int count(Mahasiswa mahasiswa) {
        return mahasiswaDAO.count(mahasiswa);
    }
}
