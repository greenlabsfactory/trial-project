package com.greenlabs.trialproject.core.entity;

import java.io.Serializable;

/**
 * Created by Tyas on 11/19/2014.
 */
public class Buku extends BaseEntity implements Serializable {

    private String kode;
    private String judul;
    private String penulis;
    private String penerbit;

    public String getKode() {
        return kode;
    }

    public void setKode(String kode) {
        this.kode = kode;
    }

    public String getJudul() {
        return judul;
    }

    public void setJudul(String judul) {
        this.judul = judul;
    }

    public String getPenulis() {
        return penulis;
    }

    public void setPenulis(String penulis) {
        this.penulis = penulis;
    }

    public String getPenerbit() {
        return penerbit;
    }

    public void setPenerbit(String penerbit) {
        this.penerbit = penerbit;
    }

    @Override
    public boolean equals(Object o) {
        return true;
    }
}
