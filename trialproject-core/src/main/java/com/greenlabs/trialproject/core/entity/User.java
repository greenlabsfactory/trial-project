/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.greenlabs.trialproject.core.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author krissadewo <dailycode.org>
 * @date Jul 18, 2013
 */
public class User implements Serializable {

    private static final long serialVersionUID = -9045973522255445037L;
    private Long id;
    private String username;
    private String password;
    private String realname;
    private String session;
    private Role role;
    private boolean delete;
    private Date deleteTime = new Date();
    private Long deleteBy;
    private List<RoleUser> roleUsers = new ArrayList<RoleUser>();
    //untuk log user
    private Long idLogUser;
    private Date timeLogin = new Date();
    private Date timeLogout = new Date();

    public User() {
    }

    public User(Long id) {
        this.id = id;
    }

    public User(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public String getRealname() {
        return realname;
    }

    public void setRealname(String realname) {
        this.realname = realname;
    }

    public boolean isDelete() {
        return delete;
    }

    public void setDelete(boolean delete) {
        this.delete = delete;
    }

    public Date getDeleteTime() {
        return deleteTime;
    }

    public void setDeleteTime(Date deleteTime) {
        this.deleteTime = deleteTime;
    }

    public Long getDeleteBy() {
        return deleteBy;
    }

    public void setDeleteBy(Long deleteBy) {
        this.deleteBy = deleteBy;
    }

    public Long getIdLogUser() {
        return idLogUser;
    }

    public void setIdLogUser(Long idLoguUser) {
        this.idLogUser = idLoguUser;
    }

    public Date getTimeLogin() {
        return timeLogin;
    }

    public void setTimeLogin(Date timeLogin) {
        this.timeLogin = timeLogin;
    }

    public Date getTimeLogout() {
        return timeLogout;
    }

    public void setTimeLogout(Date timeLogout) {
        this.timeLogout = timeLogout;
    }

    public String getSession() {
        return session;
    }

    public void setSession(String session) {
        this.session = session;
    }

    public List<RoleUser> getRoleUsers() {
        return roleUsers;
    }

    public void setRoleUsers(List<RoleUser> roleUsers) {
        this.roleUsers = roleUsers;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof User)) return false;

        User user = (User) o;

        if (delete != user.delete) return false;
        if (deleteBy != null ? !deleteBy.equals(user.deleteBy) : user.deleteBy != null) return false;
        if (deleteTime != null ? !deleteTime.equals(user.deleteTime) : user.deleteTime != null) return false;
        if (id != null ? !id.equals(user.id) : user.id != null) return false;
        if (idLogUser != null ? !idLogUser.equals(user.idLogUser) : user.idLogUser != null) return false;
        if (password != null ? !password.equals(user.password) : user.password != null) return false;
        if (realname != null ? !realname.equals(user.realname) : user.realname != null) return false;
        if (role != null ? !role.equals(user.role) : user.role != null) return false;
        if (roleUsers != null ? !roleUsers.equals(user.roleUsers) : user.roleUsers != null) return false;
        if (session != null ? !session.equals(user.session) : user.session != null) return false;
        if (timeLogin != null ? !timeLogin.equals(user.timeLogin) : user.timeLogin != null) return false;
        if (timeLogout != null ? !timeLogout.equals(user.timeLogout) : user.timeLogout != null) return false;
        if (username != null ? !username.equals(user.username) : user.username != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (username != null ? username.hashCode() : 0);
        result = 31 * result + (password != null ? password.hashCode() : 0);
        result = 31 * result + (realname != null ? realname.hashCode() : 0);
        result = 31 * result + (session != null ? session.hashCode() : 0);
        result = 31 * result + (role != null ? role.hashCode() : 0);
        result = 31 * result + (delete ? 1 : 0);
        result = 31 * result + (deleteTime != null ? deleteTime.hashCode() : 0);
        result = 31 * result + (deleteBy != null ? deleteBy.hashCode() : 0);
        result = 31 * result + (roleUsers != null ? roleUsers.hashCode() : 0);
        result = 31 * result + (idLogUser != null ? idLogUser.hashCode() : 0);
        result = 31 * result + (timeLogin != null ? timeLogin.hashCode() : 0);
        result = 31 * result + (timeLogout != null ? timeLogout.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", realname='" + realname + '\'' +
                ", session='" + session + '\'' +
                ", role=" + role +
                '}';
    }
}
