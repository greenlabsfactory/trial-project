package com.greenlabs.trialproject.core.dao;

import com.greenlabs.trialproject.core.dao.BaseDAO;
import com.greenlabs.trialproject.core.entity.Buku;

import java.util.List;

/**
 * Created by Tyas on 11/19/2014.
 */
public interface BukuDAO extends BaseDAO<Buku> {
}
