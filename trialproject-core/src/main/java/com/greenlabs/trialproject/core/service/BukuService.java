package com.greenlabs.trialproject.core.service;

import com.greenlabs.trialproject.core.AppCore;
import com.greenlabs.trialproject.core.common.Constant;
import com.greenlabs.trialproject.core.common.Result;
import com.greenlabs.trialproject.core.dao.BukuDAO;
import com.greenlabs.trialproject.core.entity.Buku;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;

import java.util.List;

/**
 * Created by Tyas on 11/19/2014.
 */
@Service
public class BukuService extends BaseService {

    @Autowired
    public BukuDAO bukuDAO;

    public Result save(final Buku buku) {
        return transactionTemplate.execute(new TransactionCallback<Result>() {
            @Override
            public Result doInTransaction(TransactionStatus status) {
                try {
                    buku.setCreatedBy(AppCore.getInstance().getUserFromSession());
                    if (buku.getId() == null) {
                        bukuDAO.save(buku);
                    } else {
                        bukuDAO.update(buku);
                    }
                } catch (DataAccessException e) {
                    status.setRollbackOnly();
                    throw e;
                }

                return null;
            }
        });
    }

    public Result delete(final Buku buku) {
        return transactionTemplate.execute(new TransactionCallback<Result>() {
            @Override
            public Result doInTransaction(TransactionStatus status) {
                try {
                    buku.setKode(buku.getKode().concat(Constant.FLAG_DELETE));
                    buku.setDeletedBy(AppCore.getInstance().getUserFromSession());
                    bukuDAO.delete(buku);
                } catch (DataAccessException e) {
                    status.setRollbackOnly();
                    throw e;
                }

                return null;
            }
        });
    }

    public Buku findById(Long id) {
        return bukuDAO.findById(id);
    }

    public List<Buku> find(Buku buku, int offset, int limit) {
        return bukuDAO.find(buku, offset, limit);
    }

    public int count(Buku buku) {
        return bukuDAO.count(buku);
    }

}
