package com.greenlabs.trialproject.core.entity;

import java.io.Serializable;

/**
 * Created by Tyas on 11/24/2014.
 */
public class Mahasiswa extends BaseEntity implements Serializable {

    private String NIM;
    private String Nama;
    private String Jurusan;
    private String Alamat;

    public String getNIM() {
        return NIM;
    }

    public void setNIM(String NIM) {
        this.NIM = NIM;
    }

    public String getNama() {
        return Nama;
    }

    public void setNama(String nama) {
        Nama = nama;
    }

    public String getJurusan() {
        return Jurusan;
    }

    public void setJurusan(String jurusan) {
        Jurusan = jurusan;
    }

    public String getAlamat() {
        return Alamat;
    }

    public void setAlamat(String alamat) {
        Alamat = alamat;
    }


}
