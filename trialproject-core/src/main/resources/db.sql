-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.5.16 - MySQL Community Server (GPL)
-- Server OS:                    Win32
-- HeidiSQL Version:             8.3.0.4694
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping database structure for trial_project
CREATE DATABASE IF NOT EXISTS `trial_project` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `trial_project`;


-- Dumping structure for table trial_project.log_user
CREATE TABLE IF NOT EXISTS `log_user` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) DEFAULT NULL,
  `session` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `time_login` datetime DEFAULT NULL,
  `time_logout` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- Dumping data for table trial_project.log_user: ~23 rows (approximately)
/*!40000 ALTER TABLE `log_user` DISABLE KEYS */;
INSERT INTO `log_user` (`id`, `user_id`, `session`, `time_login`, `time_logout`) VALUES
	(1, 1, '4034357144E4F9A8BEB059CDE04761CC', '2014-07-07 14:14:08', '2014-07-07 14:14:10'),
	(2, 1, 'D4CEE59B6364F4E797A99D8803398D01', '2014-07-07 14:27:15', '2014-07-07 15:01:15'),
	(3, 1, '23FC0ED2BC106A81B116716306185A15', '2014-07-07 14:37:31', '2014-07-07 15:11:32'),
	(4, 1, '5CA4F02E271CFF89E9CDD598B7D53AA5', '2014-07-07 15:49:46', NULL),
	(5, 1, 'F3718813D953CC6FA6E524953076F11F', '2014-07-07 16:13:35', NULL),
	(6, 1, '30AEFD11C721D5B000E2BD3113A3B4D3', '2014-07-07 16:16:12', NULL),
	(7, 1, 'F86C7C2256119728CA75AA439291CB3D', '2014-07-07 17:58:03', NULL),
	(8, 1, 'F085B942C938403ECC5124BE2624610E', '2014-07-08 08:22:04', '2014-07-08 09:32:45'),
	(9, 1, 'CE922FAE03F520E233A468FA613FF0D3', '2014-07-08 09:32:48', '2014-07-08 11:02:34'),
	(10, 1, 'D3FFD708BEC5B7C7B121CAA7A177B818', '2014-07-08 11:02:38', '2014-07-08 13:29:40'),
	(11, 1, '8C5C91B5487A448207324999078029F8', '2014-07-08 11:19:28', NULL),
	(12, 1, 'FE98215FD9FB787441EF07C4402BE810', '2014-07-08 13:29:44', NULL),
	(13, 1, 'D987DA8887871569ADF604450BA3C6C1', '2014-07-08 13:37:25', '2014-07-08 14:45:01'),
	(14, 1, 'CE41D19ACE039A388A39B1E68E0D1105', '2014-07-08 14:45:06', '2014-07-08 15:53:09'),
	(15, 1, 'EFACC925C65D34FF55983A565CD818CE', '2014-07-08 16:04:09', NULL),
	(16, 1, '83D67482581203399B4C6D11C120FBF4', '2014-07-08 19:21:32', NULL),
	(17, 1, '47A7ADC1F27157672952F85E901A07CE', '2014-07-10 08:19:51', NULL),
	(18, 1, '006447DBA8E104EE7E30D3AF27CE7D55', '2014-07-10 08:56:20', '2014-07-10 09:30:21'),
	(19, 1, '570A57BB40B0682328F2AF38E1E2ADC9', '2014-07-10 10:15:46', '2014-07-10 11:30:14'),
	(20, 1, 'A1BB58BE88525EB0A2F9D0D4AFEC27F4', '2014-07-10 11:30:17', '2014-07-10 13:17:49'),
	(21, 1, '328D4409E7C0232EA4AA34A467A7DD7A', '2014-07-10 13:17:52', NULL),
	(22, 1, '2666EBEDEE2E73B403C43FD4F38B0D7E', '2014-07-10 13:36:41', NULL),
	(23, 1, '3EEDEA2893B3D5BC8888BB3C744CAD35', '2014-07-10 13:54:03', '2014-09-10 11:43:14');
/*!40000 ALTER TABLE `log_user` ENABLE KEYS */;


-- Dumping structure for table trial_project.master_agama
CREATE TABLE IF NOT EXISTS `master_agama` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'PK master_agama',
  `nama` varchar(255) CHARACTER SET latin1 DEFAULT NULL COMMENT 'nama agama',
  `created_by` bigint(20) unsigned DEFAULT NULL,
  `created_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nama` (`nama`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci COMMENT='Tabel master agama';

-- Dumping data for table trial_project.master_agama: ~1 rows (approximately)
/*!40000 ALTER TABLE `master_agama` DISABLE KEYS */;
INSERT INTO `master_agama` (`id`, `nama`, `created_by`, `created_time`) VALUES
	(29, 'ISLAM', 1, '2014-09-10 11:43:27');
/*!40000 ALTER TABLE `master_agama` ENABLE KEYS */;


-- Dumping structure for table trial_project.system_menu
CREATE TABLE IF NOT EXISTS `system_menu` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `kode` varchar(50) CHARACTER SET latin1 NOT NULL,
  `title` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `nama` varchar(255) CHARACTER SET latin1 NOT NULL,
  `order_number` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- Dumping data for table trial_project.system_menu: ~5 rows (approximately)
/*!40000 ALTER TABLE `system_menu` DISABLE KEYS */;
INSERT INTO `system_menu` (`id`, `parent_id`, `kode`, `title`, `nama`, `order_number`) VALUES
	(1, 0, 'VIEW_ROOT_SETTING', 'Setting', 'Setting', 5),
	(2, 1, 'VIEW_SETTING_USER_MANAGEMENT', 'User', 'User', 2),
	(3, 1, 'VIEW_SETTING_HAK_AKSES', 'User Level', 'User Level', 3),
	(4, 0, 'VIEW_ROOT_MASTER', 'Master', 'Master', 0),
	(5, 4, 'VIEW_MASTER_AGAMA', 'Agama', 'Agama', 0);
/*!40000 ALTER TABLE `system_menu` ENABLE KEYS */;


-- Dumping structure for table trial_project.system_parameter
CREATE TABLE IF NOT EXISTS `system_parameter` (
  `media_server` varchar(255) CHARACTER SET latin1 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- Dumping data for table trial_project.system_parameter: ~1 rows (approximately)
/*!40000 ALTER TABLE `system_parameter` DISABLE KEYS */;
INSERT INTO `system_parameter` (`media_server`) VALUES
	('http://192.168.0.9/');
/*!40000 ALTER TABLE `system_parameter` ENABLE KEYS */;


-- Dumping structure for table trial_project.system_role
CREATE TABLE IF NOT EXISTS `system_role` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- Dumping data for table trial_project.system_role: ~6 rows (approximately)
/*!40000 ALTER TABLE `system_role` DISABLE KEYS */;
INSERT INTO `system_role` (`id`, `nama`) VALUES
	(1, 'ADMIN'),
	(2, 'KASIR'),
	(3, 'ACCOUNTING'),
	(4, 'KEUANGAN'),
	(5, 'SALES ENTRY'),
	(6, 'MANAGEMENT');
/*!40000 ALTER TABLE `system_role` ENABLE KEYS */;


-- Dumping structure for table trial_project.system_role_menu
CREATE TABLE IF NOT EXISTS `system_role_menu` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `menu_id` bigint(20) unsigned NOT NULL,
  `role_id` bigint(20) unsigned NOT NULL,
  `can_read` tinyint(4) DEFAULT '0',
  `can_edit` tinyint(4) DEFAULT '0',
  `can_save` tinyint(4) DEFAULT '0',
  `can_delete` tinyint(4) DEFAULT '0',
  `can_print` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- Dumping data for table trial_project.system_role_menu: ~6 rows (approximately)
/*!40000 ALTER TABLE `system_role_menu` DISABLE KEYS */;
INSERT INTO `system_role_menu` (`id`, `menu_id`, `role_id`, `can_read`, `can_edit`, `can_save`, `can_delete`, `can_print`) VALUES
	(1, 1, 1, 1, 1, 1, 1, 0),
	(2, 2, 1, 1, 1, 1, 1, 0),
	(3, 3, 1, 1, 1, 1, 1, 0),
	(11, 63, 1, 1, 0, 0, 0, 0),
	(12, 4, 1, 1, 0, 0, 0, 0),
	(13, 5, 1, 1, 1, 1, 1, 1);
/*!40000 ALTER TABLE `system_role_menu` ENABLE KEYS */;


-- Dumping structure for table trial_project.system_role_user
CREATE TABLE IF NOT EXISTS `system_role_user` (
  `id` bigint(10) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(10) NOT NULL,
  `role_id` bigint(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- Dumping data for table trial_project.system_role_user: ~7 rows (approximately)
/*!40000 ALTER TABLE `system_role_user` DISABLE KEYS */;
INSERT INTO `system_role_user` (`id`, `user_id`, `role_id`) VALUES
	(1, 2, 2),
	(2, 2, 3),
	(3, 2, 1),
	(4, 1, 1),
	(5, 4, 1),
	(6, 7, 2),
	(7, 7, 3);
/*!40000 ALTER TABLE `system_role_user` ENABLE KEYS */;


-- Dumping structure for table trial_project.system_user
CREATE TABLE IF NOT EXISTS `system_user` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `realname` varchar(255) CHARACTER SET latin1 NOT NULL,
  `username` varchar(255) CHARACTER SET latin1 NOT NULL,
  `password` varchar(255) CHARACTER SET latin1 NOT NULL,
  `is_delete` enum('Y','N') CHARACTER SET latin1 NOT NULL DEFAULT 'N',
  `delete_by` bigint(20) DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `role_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- Dumping data for table trial_project.system_user: ~4 rows (approximately)
/*!40000 ALTER TABLE `system_user` DISABLE KEYS */;
INSERT INTO `system_user` (`id`, `realname`, `username`, `password`, `is_delete`, `delete_by`, `delete_time`, `role_id`) VALUES
	(1, 'SYSTEM', 'USERNAME', 'DMBtF+6HVzmxVnvQ8xuGCzO7YputDBA7G3lzsSRpjaVw8CqtIConQ2NUq2kv6b6P', 'N', NULL, NULL, 1),
	(2, 'kris', 'kris', 'py5pNpGrvk9Yqq5ncfI3YF4CyvcI+Apqh0AnfIzg+enbXRtDTL5VwlO6UQ5EbLJF', 'N', NULL, NULL, 1),
	(4, 'admin', 'admin', 'tTZNXysInE74NSyWoqvNWZkLGKwGDukfi+UFTC3eM8SCXwAna2vHQCU+wfG+TCF9', 'N', NULL, NULL, 1),
	(7, 'x1', 'x1', 'qr5GNBHEp6DJm5m8jorftcC7umVym0uMzh9FdqiBaCy4QZzoC7tgODsQ7z0imFm9', 'N', NULL, NULL, 1);
/*!40000 ALTER TABLE `system_user` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
