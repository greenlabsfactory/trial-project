/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.greenlabs.trialproject.web.client.view.menu.settings;

import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.greenlabs.trialproject.web.client.AppClient;
import com.greenlabs.trialproject.web.client.view.View;
import com.greenlabs.trialproject.web.client.view.custom.ComboBoxRole;
import com.greenlabs.trialproject.web.client.view.custom.CustomFieldLabel;
import com.greenlabs.trialproject.web.client.view.custom.WindowFormView;
import com.greenlabs.trialproject.web.client.view.handler.FormHandler;
import com.greenlabs.trialproject.core.common.Result;
import com.greenlabs.trialproject.core.entity.Role;
import com.greenlabs.trialproject.core.entity.RoleUser;
import com.greenlabs.trialproject.core.entity.User;
import com.sencha.gxt.widget.core.client.container.VerticalLayoutContainer;
import com.sencha.gxt.widget.core.client.event.SelectEvent;
import com.sencha.gxt.widget.core.client.form.CheckBox;
import com.sencha.gxt.widget.core.client.form.ComboBox;
import com.sencha.gxt.widget.core.client.form.PasswordField;
import com.sencha.gxt.widget.core.client.form.TextField;

import java.util.ArrayList;
import java.util.List;

/**
 * @author krissadewo
 */
public class UserFormView extends View implements FormHandler {

    private WindowFormView windowFormView;
    private UserView parentView;
    private TextField textFieldNamaAsli;
    private TextField textFieldUsername;
    private PasswordField textFieldPassword;
    private ComboBox<Role> comboBoxRole;
    private User user;
    private VerticalLayoutContainer roleContainer = new VerticalLayoutContainer();
    private List<Role> roles = new ArrayList<>();

    @Override
    public void showForm(View view) {
        parentView = (UserView) view;
        windowFormView = new WindowFormView(parentView.getGrid());

        textFieldNamaAsli = new TextField();
        textFieldNamaAsli.setAllowBlank(false);

        textFieldUsername = new TextField();
        textFieldUsername.setAllowBlank(false);

        textFieldPassword = new PasswordField();
        textFieldPassword.setAllowBlank(false);

        comboBoxRole = (ComboBox) new ComboBoxRole().asWidget();

        if (parentView.getGrid().getSelectionModel().getSelectedItem() != null) {
            windowFormView.setHeadingText("Ubah Data");
            user = parentView.getGrid().getSelectionModel().getSelectedItem();
            textFieldNamaAsli.setValue(user.getRealname());
            textFieldUsername.setValue(user.getUsername());
            textFieldPassword.setValue(user.getPassword());
            comboBoxRole.setValue(user.getRole());
        } else {
            user = new User();
        }

        initRole(user);

        windowFormView.addWidget(new CustomFieldLabel(textFieldNamaAsli, "Nama Asli"), new VerticalLayoutContainer.VerticalLayoutData(1, -1));
        windowFormView.addWidget(new CustomFieldLabel(textFieldUsername, "Username"), new VerticalLayoutContainer.VerticalLayoutData(1, -1));
        windowFormView.addWidget(new CustomFieldLabel(textFieldPassword, "Password"), new VerticalLayoutContainer.VerticalLayoutData(1, -1));
        windowFormView.addWidget(new CustomFieldLabel(roleContainer, "Role"), new VerticalLayoutContainer.VerticalLayoutData(1, -1));
        windowFormView.getButtonSave().addSelectHandler(buttonSaveSelectHandler());
        windowFormView.setCursorPosition(textFieldNamaAsli);
        windowFormView.show();
    }

    private void initRole(final User user) {

        getService().getRoleServiceAsync().findAll(new AsyncCallback<ArrayList<Role>>() {
            @Override
            public void onFailure(Throwable caught) {
            }

            @Override
            public void onSuccess(ArrayList<Role> result) {
                roles = result;
                for (final Role role : roles) {
                    final CheckBox checkBox = new CheckBox();
                    checkBox.setBoxLabel(role.getNama());
                    checkBox.addValueChangeHandler(new ValueChangeHandler<Boolean>() {
                        @Override
                        public void onValueChange(ValueChangeEvent<Boolean> booleanValueChangeEvent) {
                            if (booleanValueChangeEvent.getValue()) { //default checked checkbox is false
                                checkBox.setData(role.getNama(), role);
                            } else {
                                checkBox.setData(role.getNama(), null);
                            }
                        }
                    });

                    //checked value to related role while edit user happen
                    getService().getRoleUserServiceAsync().find(user, new AsyncCallback<ArrayList<RoleUser>>() {
                        @Override
                        public void onFailure(Throwable caught) {
                        }

                        @Override
                        public void onSuccess(ArrayList<RoleUser> result) {
                            for (RoleUser roleUser : result) {
                                if (roleUser.getRole().getId().equals(role.getId())) {
                                    checkBox.setValue(true);
                                    return;
                                }
                            }
                        }
                    });

                    roleContainer.add(checkBox);
                }

                windowFormView.forceLayout();
            }
        });
    }

    @Override
    public SelectEvent.SelectHandler buttonSaveSelectHandler() {
        return new SelectEvent.SelectHandler() {
            @Override
            public void onSelect(SelectEvent event) {
                if (!windowFormView.getFormPanel().isValid()) {
                    return;
                }

                user.setRealname(textFieldNamaAsli.getValue());
                user.setUsername(textFieldUsername.getValue());
                user.setPassword(textFieldPassword.getValue());
                user.setRole(comboBoxRole.getCurrentValue());

                int i = 0;
                List<RoleUser> roleUsers = new ArrayList<>();
                for (Role role : roles) {
                    CheckBox checkBox = (CheckBox) roleContainer.getWidget(i);
                    if (checkBox.getData(role.getNama()) != null) {
                        RoleUser roleUser = new RoleUser();
                        roleUser.setRole(role);
                        roleUsers.add(roleUser);
                    }
                    i++;
                }

                user.setRoleUsers(roleUsers);

                getService().getUserServiceAsync().save(user, new AsyncCallback<Result>() {

                    @Override
                    public void onFailure(Throwable caught) {
                        AppClient.showMessageOnFailureException(caught);
                    }

                    @Override
                    public void onSuccess(Result result) {
                        if (result.getMessage().equals(Result.SAVE_SUCCESS)) {
                            windowFormView.hide();
                            windowFormView.getFormPanel().reset();
                            parentView.initGrid();
                        } else {
                            windowFormView.getButtonSave().setEnabled(true);
                        }

                        AppClient.showInfoMessage(result.getMessage());
                    }
                });
            }
        };
    }
}
