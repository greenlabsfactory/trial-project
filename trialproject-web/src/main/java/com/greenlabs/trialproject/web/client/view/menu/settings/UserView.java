/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.greenlabs.trialproject.web.client.view.menu.settings;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Widget;
import com.greenlabs.trialproject.web.client.AppClient;
import com.greenlabs.trialproject.web.client.view.View;
import com.greenlabs.trialproject.web.client.view.custom.GridView;
import com.greenlabs.trialproject.web.client.view.custom.ToolbarGridView;
import com.greenlabs.trialproject.web.client.view.handler.GridHandler;
import com.greenlabs.trialproject.core.common.Result;
import com.greenlabs.trialproject.core.entity.Menu;
import com.greenlabs.trialproject.core.entity.User;
import com.sencha.gxt.data.shared.ListStore;
import com.sencha.gxt.widget.core.client.Dialog;
import com.sencha.gxt.widget.core.client.box.ConfirmMessageBox;
import com.sencha.gxt.widget.core.client.container.VerticalLayoutContainer;
import com.sencha.gxt.widget.core.client.event.HideEvent;
import com.sencha.gxt.widget.core.client.event.RowDoubleClickEvent;
import com.sencha.gxt.widget.core.client.event.SelectEvent;
import com.sencha.gxt.widget.core.client.grid.ColumnConfig;
import com.sencha.gxt.widget.core.client.grid.ColumnModel;
import com.sencha.gxt.widget.core.client.grid.Grid;
import com.sencha.gxt.widget.core.client.grid.filters.GridFilters;
import com.sencha.gxt.widget.core.client.grid.filters.StringFilter;

import java.util.ArrayList;

/**
 * @author krissadewo
 */
public class UserView extends View implements GridHandler {

    private Grid<User> grid;
    private ListStore<User> listStore;

    public UserView(Menu menu) {
        super(menu);
    }

    @Override
    public Widget asWidget() {
        listStore = new ListStore<>(getProperties().getUserProperties().key());

        ColumnConfig<User, String> colRealName = new ColumnConfig<>(getProperties().getUserProperties().valueRealName(), 200, "NAME");
        ColumnConfig<User, String> colUsername = new ColumnConfig<>(getProperties().getUserProperties().valueUsername(), 200, "USERNAME");
        ColumnConfig<User, String> colNamaLevel = new ColumnConfig<>(getProperties().getUserProperties().valueNamaRole(), 100, "ROLE");

        ArrayList<ColumnConfig<User, ?>> list = new ArrayList<>();
        list.add(colRealName);
        list.add(colUsername);
        list.add(colNamaLevel);
        ColumnModel<User> columnModel = new ColumnModel<>(list);

        grid = new Grid<>(listStore, columnModel);
        grid.getView().setStripeRows(true);
        grid.getView().setColumnLines(true);
        grid.getView().setAutoFill(true);
        grid.addRowDoubleClickHandler(gridRowDoubleClickHandler());

        StringFilter<User> namaFilter = new StringFilter<>(getProperties().getUserProperties().valueRealName());
        StringFilter<User> usernameFilter = new StringFilter<>(getProperties().getUserProperties().valueUsername());
        StringFilter<User> levelFilter = new StringFilter<>(getProperties().getUserProperties().valueNamaRole());

        GridFilters<User> gridFilters = new GridFilters<>();
        gridFilters.initPlugin(grid);
        gridFilters.setLocal(true);
        gridFilters.addFilter(namaFilter);
        gridFilters.addFilter(usernameFilter);
        gridFilters.addFilter(levelFilter);

        initGrid();

        GridView gridView = new GridView();
        gridView.addWidget(createToolbar(), new VerticalLayoutContainer.VerticalLayoutData(1, -1));
        gridView.addWidget(grid, new VerticalLayoutContainer.VerticalLayoutData(1, 1));
        return gridView;
    }

    private Widget createToolbar() {
        return new ToolbarGridView(super.getMenu()) {
            @Override
            public SelectEvent.SelectHandler buttonToolbarAddSelectHandler() {
                return buttonAddSelectHandler();
            }

            @Override
            public SelectEvent.SelectHandler buttonToolbarEditSelectHandler() {
                return buttonEditSelectHandler();
            }

            @Override
            public SelectEvent.SelectHandler buttonToolbarDeleteSelectHandler() {
                return buttonDeleteSelectHandler();
            }

            @Override
            public SelectEvent.SelectHandler buttonToolbarSearchSelectHandler() {
                return buttonSearchSelectHandler();
            }

            @Override
            public SelectEvent.SelectHandler buttonToolbarPrintSelectHandler() {
                return buttonPrintSelectHandler();
            }
        }.asWidget();
    }

    public void initGrid() {
        getService().getUserServiceAsync().findAll(new AsyncCallback<ArrayList<User>>() {
            @Override
            public void onFailure(Throwable caught) {
                AppClient.showMessageOnFailureException(caught);
            }

            @Override
            public void onSuccess(ArrayList<User> result) {
                listStore.replaceAll(result);
            }
        });
    }

    @Override
    public SelectEvent.SelectHandler buttonAddSelectHandler() {
        return new SelectEvent.SelectHandler() {
            @Override
            public void onSelect(SelectEvent event) {
                grid.getSelectionModel().deselectAll();
                new UserFormView().showForm(UserView.this);
            }
        };
    }

    @Override
    public SelectEvent.SelectHandler buttonEditSelectHandler() {
        return new SelectEvent.SelectHandler() {
            @Override
            public void onSelect(SelectEvent event) {
                if (grid.getSelectionModel().getSelectedItem() != null) {
                    new UserFormView().showForm(UserView.this);
                } else {
                    AppClient.showInfoMessage(Result.DATA_NOT_SELECTED);
                }
                grid.getSelectionModel().deselectAll();
            }
        };
    }

    @Override
    public SelectEvent.SelectHandler buttonDeleteSelectHandler() {
        return new SelectEvent.SelectHandler() {
            @Override
            public void onSelect(SelectEvent event) {
                final User user = grid.getSelectionModel().getSelectedItem();
                if (user == null) {
                    AppClient.showInfoMessage(Result.DATA_NOT_SELECTED);
                    return;
                }

                ConfirmMessageBox confirmMessageBox = AppClient.showMessageConfirmForDelete();
                confirmMessageBox.addHideHandler(new HideEvent.HideHandler() {
                    @Override
                    public void onHide(HideEvent event) {
                        Dialog dialog = (Dialog) event.getSource();
                        if (dialog.getDialogMessages().yes().equals(AppClient.MESSAGE_YES)) {
                            getService().getUserServiceAsync().delete(user, new AsyncCallback<Result>() {
                                @Override
                                public void onFailure(Throwable caught) {
                                }

                                @Override
                                public void onSuccess(Result result) {
                                    AppClient.showInfoMessage(result.getMessage());
                                    if (result.getMessage().equals(Result.DELETE_SUCCESS)) {
                                        initGrid();
                                    }
                                }
                            });
                        } else {
                            grid.getSelectionModel().deselectAll();
                        }
                    }
                });
                confirmMessageBox.show();
            }
        };
    }

    @Override
    public SelectEvent.SelectHandler buttonSearchSelectHandler() {
        return new SelectEvent.SelectHandler() {
            @Override
            public void onSelect(SelectEvent event) {
                AppClient.showInfoMessage(Result.SEARCH_NOT_HERE);
            }
        };
    }

    @Override
    public SelectEvent.SelectHandler buttonPrintSelectHandler() {
        return new SelectEvent.SelectHandler() {
            @Override
            public void onSelect(SelectEvent event) {
                //To change body of implemented methods use File | Settings | File Templates.
            }
        };
    }

    @Override
    public RowDoubleClickEvent.RowDoubleClickHandler gridRowDoubleClickHandler() {
        return new RowDoubleClickEvent.RowDoubleClickHandler() {
            @Override
            public void onRowDoubleClick(RowDoubleClickEvent event) {
                new UserFormView().showForm(UserView.this);
            }
        };
    }


    public Grid<User> getGrid() {
        return grid;
    }


}
