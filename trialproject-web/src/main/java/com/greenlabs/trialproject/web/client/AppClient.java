/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.greenlabs.trialproject.web.client;

import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.resources.client.ImageResource;
import com.greenlabs.trialproject.core.common.Result;
import com.sencha.gxt.widget.core.client.box.AlertMessageBox;
import com.sencha.gxt.widget.core.client.box.ConfirmMessageBox;
import com.sencha.gxt.widget.core.client.box.MessageBox;
import com.sencha.gxt.widget.core.client.event.HideEvent;
import com.sencha.gxt.widget.core.client.toolbar.PagingToolBar;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * @author krissadewo
 */
public class AppClient {

    public static final String MESSAGE_YES = "YES";
    public static final Integer PAGING_SIZE = 100;
    public static final NumberFormat CURRENCY_FORMAT = NumberFormat.getFormat("#,###,###,##0.00");

    public static AppClient getInstance() {
        return AppClientHolder.INSTANCE;
    }

    public static void showMessageOnFailureException(Throwable throwable) {
        AlertMessageBox box = new AlertMessageBox("Error", "Mungkin terjadi masalah pada jaringan anda, periksa lagi apakah jaringan anda...");
        box.setIcon(MessageBox.ICONS.error());
        box.show();
    }

    public static ConfirmMessageBox showMessageConfirmForDelete() {
        return new ConfirmMessageBox("Confirm", "Hapus data ini ?");
    }

    public static ConfirmMessageBox showMessageConfirmForDelete(String dataInfo) {
        return new ConfirmMessageBox("Confirm", "Hapus data ini ?\n" + dataInfo);
    }

    public static MessageBox showInfoMessage(Result message) {
        AlertMessageBox box = new AlertMessageBox("Informasi", message.getMessage());
        box.setIcon(MessageBox.ICONS.info());
        box.show();
        return box;
    }


    public static MessageBox showCustomMessage(String title, String message, ImageResource icon) {
        AlertMessageBox box = new AlertMessageBox(title, message);
        box.setIcon(icon);
        box.show();
        return box;
    }

    /**
     * @param message
     * @param pagingToolBar
     * @return
     */
    public static MessageBox showInfoMessage(String message, final PagingToolBar pagingToolBar) {
        AlertMessageBox box = new AlertMessageBox("Informasi", message);
        box.setIcon(MessageBox.ICONS.info());
        box.addHideHandler(new HideEvent.HideHandler() {
            @Override
            public void onHide(HideEvent event) {
                pagingToolBar.refresh();
            }
        });
        box.show();
        return box;
    }

    /**
     * @param message
     * @return
     */
    public static MessageBox showInfoMessage(String message) {
        AlertMessageBox box = new AlertMessageBox("Informasi", message);
        box.setIcon(MessageBox.ICONS.info());
        box.show();
        return box;
    }

    /**
     * This is standard method to divide BigDecimal value
     *
     * @param x x
     * @param y y
     * @return devide value
     */
    public static BigDecimal divide(BigDecimal x, BigDecimal y) {
        return x.divide(y, 2, RoundingMode.HALF_EVEN);
    }

    private static class AppClientHolder {

        private static final AppClient INSTANCE = new AppClient();
    }
}
