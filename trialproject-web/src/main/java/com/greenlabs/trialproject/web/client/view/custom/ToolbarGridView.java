/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.greenlabs.trialproject.web.client.view.custom;

import com.greenlabs.trialproject.core.entity.Menu;
import com.greenlabs.trialproject.web.client.icon.Icon;
import com.sencha.gxt.cell.core.client.ButtonCell;
import com.sencha.gxt.core.client.util.Margins;
import com.sencha.gxt.widget.core.client.button.TextButton;
import com.sencha.gxt.widget.core.client.container.BoxLayoutContainer;
import com.sencha.gxt.widget.core.client.container.HBoxLayoutContainer;
import com.sencha.gxt.widget.core.client.event.SelectEvent;

/**
 * @author krissadewo
 */
public abstract class ToolbarGridView extends HBoxLayoutContainer {

    private String textButtonWidth = "80";
    private BoxLayoutData layoutData = new BoxLayoutContainer.BoxLayoutData(new Margins(5, 5, 5, 0));


    public ToolbarGridView(Menu menu) {
        init(menu, null);
    }

    /**
     * @param menu        menu ref
     * @param toolbarType specific toolbar type, can be null
     */
    public ToolbarGridView(Menu menu, ToolbarType toolbarType) {
        init(menu, toolbarType);
    }

    private void init(Menu menu, ToolbarType toolbarType) {
        super.setPack(BoxLayoutContainer.BoxLayoutPack.END);

        //We can custom anything at here
        if (menu.getRoleMenu().isCanSave()) {
            TextButton textButtonAdd = new TextButton("Tambah", buttonToolbarAddSelectHandler());
            textButtonAdd.setIcon(Icon.INSTANCE.add());
            textButtonAdd.setIconAlign(ButtonCell.IconAlign.RIGHT);
            textButtonAdd.setWidth(textButtonWidth);
            super.add(textButtonAdd, layoutData);
        }

        if (menu.getRoleMenu().isCanEdit()) {
            TextButton textButtonEdit = new TextButton("Ubah", buttonToolbarEditSelectHandler());
            textButtonEdit.setIcon(Icon.INSTANCE.edit());
            textButtonEdit.setIconAlign(ButtonCell.IconAlign.RIGHT);
            textButtonEdit.setWidth(textButtonWidth);
            super.add(textButtonEdit, layoutData);
        }

        if (menu.getRoleMenu().isCanDelete()) {
            TextButton textButtonDelete = new TextButton("Hapus", buttonToolbarDeleteSelectHandler());
            textButtonDelete.setIcon(Icon.INSTANCE.delete());
            textButtonDelete.setIconAlign(ButtonCell.IconAlign.RIGHT);
            textButtonDelete.setWidth(textButtonWidth);
            super.add(textButtonDelete, layoutData);
        }

        if (toolbarType == ToolbarType.CUSTOM_TOOLBAR) {
            TextButton textButtonSync = new TextButton("Custom Toolbar", buttonToolbarSyncSelectHandler());
            textButtonSync.setIcon(Icon.INSTANCE.sync());
            textButtonSync.setIconAlign(ButtonCell.IconAlign.RIGHT);
            textButtonSync.setWidth(textButtonWidth);
            super.add(textButtonSync, layoutData);
        }

        TextButton textButtonSearch = new TextButton("Cari", buttonToolbarSearchSelectHandler());
        textButtonSearch.setIcon(Icon.INSTANCE.search());
        textButtonSearch.setIconAlign(ButtonCell.IconAlign.RIGHT);
        textButtonSearch.setWidth(textButtonWidth);
        super.add(textButtonSearch, layoutData);

        if (menu.getRoleMenu().isCanPrint()) {
            TextButton textButtonPrint = new TextButton("Unduh", buttonToolbarPrintSelectHandler());
            textButtonPrint.setIcon(Icon.INSTANCE.exportExcel());
            textButtonPrint.setIconAlign(ButtonCell.IconAlign.RIGHT);
            textButtonPrint.setWidth(textButtonWidth);
            super.add(textButtonPrint, layoutData);
        }
    }

    public abstract SelectEvent.SelectHandler buttonToolbarAddSelectHandler();

    public abstract SelectEvent.SelectHandler buttonToolbarEditSelectHandler();

    public abstract SelectEvent.SelectHandler buttonToolbarDeleteSelectHandler();

    public abstract SelectEvent.SelectHandler buttonToolbarSearchSelectHandler();

    public abstract SelectEvent.SelectHandler buttonToolbarPrintSelectHandler();

    public SelectEvent.SelectHandler buttonToolbarSyncSelectHandler() {
        return new SelectEvent.SelectHandler() {
            @Override
            public void onSelect(SelectEvent event) {
            }
        };
    }
}
