package com.greenlabs.trialproject.web.client.service;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.greenlabs.trialproject.core.common.Result;
import com.greenlabs.trialproject.core.entity.Buku;
import com.sencha.gxt.data.shared.loader.PagingLoadConfig;
import com.sencha.gxt.data.shared.loader.PagingLoadResult;

import java.util.ArrayList;

/**
 * Created by Tyas on 11/21/2014.
 */
public interface GwtBukuServiceAsync {
    void save(Buku buku, AsyncCallback<Result> callback);

    void delete(Buku buku, AsyncCallback<Result> callback);

    void find(Buku buku, PagingLoadConfig pagingLoadConfig, AsyncCallback<PagingLoadResult<Buku>> callback);

    void find(Buku buku, AsyncCallback<ArrayList<Buku>> callback);
}
