/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.greenlabs.trialproject.web.client.view.custom;

import com.google.gwt.user.client.ui.Widget;
import com.greenlabs.trialproject.web.client.view.View;
import com.greenlabs.trialproject.core.common.CustomField;
import com.greenlabs.trialproject.core.common.Hari;
import com.sencha.gxt.cell.core.client.form.ComboBoxCell;
import com.sencha.gxt.data.shared.ListStore;
import com.sencha.gxt.widget.core.client.form.ComboBox;

import java.util.ArrayList;
import java.util.List;

/**
 * @author krissadewo
 *         <p/>
 *         The generic way to fill for any requirement for combo box
 */
public class ComboBoxCustom extends View {

    private ComboBoxType comboBoxType;
    private CustomField customField;

    public ComboBoxCustom(ComboBoxType comboBoxType) {
        this.comboBoxType = comboBoxType;
    }

    public ComboBoxCustom(ComboBoxType comboBoxType, CustomField customField) {
        this.comboBoxType = comboBoxType;
        this.customField = customField;
    }

    @Override
    public Widget asWidget() {
        ListStore<CustomField> listStore = new ListStore<>(getProperties().getCustomFieldProperties().key());
        ComboBox<CustomField> comboBox = new ComboBox<>(listStore, getProperties().getCustomFieldProperties().labelName());

        List<CustomField> customFields = new ArrayList<>();
        if (comboBoxType == ComboBoxType.HARI) {
            customFields.add(new CustomField(Hari.SENIN.toString(), Hari.SENIN.toString()));
            customFields.add(new CustomField(Hari.SELASA.toString(), Hari.SELASA.toString()));
            customFields.add(new CustomField(Hari.RABU.toString(), Hari.RABU.toString()));
            customFields.add(new CustomField(Hari.KAMIS.toString(), Hari.KAMIS.toString()));
            customFields.add(new CustomField(Hari.JUMAT.toString(), Hari.JUMAT.toString()));
            customFields.add(new CustomField(Hari.SABTU.toString(), Hari.SABTU.toString()));
            customFields.add(new CustomField(Hari.MINGGU.toString(), Hari.MINGGU.toString()));

        }

        if (customField != null) {
            comboBox.setValue(customField);
        } else {
            comboBox.setValue(customFields.get(0));
        }

        listStore.replaceAll(customFields);

        comboBox.setEditable(false);
        comboBox.setOriginalValue(comboBox.getValue());
        comboBox.setAutoValidate(true);
        comboBox.setEmptyText("Pilih Data");
        comboBox.setTriggerAction(ComboBoxCell.TriggerAction.ALL);
        comboBox.setForceSelection(true);
        return comboBox;
    }

}
