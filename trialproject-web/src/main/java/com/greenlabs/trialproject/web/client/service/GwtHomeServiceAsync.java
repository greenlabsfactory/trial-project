package com.greenlabs.trialproject.web.client.service;

import com.google.gwt.user.client.rpc.AsyncCallback;

public interface GwtHomeServiceAsync {

    void getAppTitle(AsyncCallback<String> callback);
}
