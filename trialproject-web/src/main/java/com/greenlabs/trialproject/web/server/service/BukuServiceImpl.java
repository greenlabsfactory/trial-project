package com.greenlabs.trialproject.web.server.service;

import com.greenlabs.trialproject.web.client.service.GwtBukuService;
import com.greenlabs.trialproject.core.common.Result;
import com.greenlabs.trialproject.core.entity.Buku;
import com.greenlabs.trialproject.core.service.BukuService;
import com.sencha.gxt.data.shared.loader.PagingLoadConfig;
import com.sencha.gxt.data.shared.loader.PagingLoadResult;
import com.sencha.gxt.data.shared.loader.PagingLoadResultBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

/**
 * Created by Tyas on 11/21/2014.
 */
@Service("gwtBukuService")
public class BukuServiceImpl implements GwtBukuService {

    @Autowired
    private BukuService bukuService;

    @Override
    public Result save(Buku buku) {
        return bukuService.save(buku);
    }

    @Override
    public Result delete(Buku buku) {
        return bukuService.delete(buku);
    }

    @Override
    public PagingLoadResult<Buku> find(Buku buku, PagingLoadConfig pagingLoadConfig) {
        return new PagingLoadResultBean<>(bukuService.find(buku, pagingLoadConfig.getOffset(),
                pagingLoadConfig.getLimit()),
                bukuService.count(buku), pagingLoadConfig.getOffset()
        );
    }

    @Override
    public ArrayList<Buku> find(Buku buku) {
        return new ArrayList<>(bukuService.find(buku, 0, Integer.MAX_VALUE));
    }
}
