/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.greenlabs.trialproject.web.client.view.custom;

import com.sencha.gxt.widget.core.client.button.TextButton;

/**
 * @author krissadewo <dailycode.org>
 * @date Jul 15, 2013
 */
public class TextButtonFilter extends TextButton {

    public TextButtonFilter() {
        super("Filter");
    }
}
