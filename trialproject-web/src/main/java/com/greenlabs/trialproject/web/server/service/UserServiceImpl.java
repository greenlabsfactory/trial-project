/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.greenlabs.trialproject.web.server.service;

import com.greenlabs.trialproject.web.client.service.GwtUserService;
import com.greenlabs.trialproject.core.common.Result;
import com.greenlabs.trialproject.core.entity.User;
import com.greenlabs.trialproject.core.service.UserService;
import com.sencha.gxt.data.shared.loader.PagingLoadConfig;
import com.sencha.gxt.data.shared.loader.PagingLoadResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

/**
 * @author krissadewo
 */
@Service("gwtUserService")
public class UserServiceImpl implements GwtUserService {

    @Autowired
    private UserService service;

    @Override
    public boolean isAuthenticatedUser() {
        return service.isAuthenticatedUser();
    }

    @Override
    public User doLogin(User user) {
        return service.doLogin(user);
    }

    @Override
    public void doLogout() {
        service.doLogout();
    }

    @Override
    public User getUserFromSession() {
        return service.getUserFromSession();
    }

    @Override
    public ArrayList<User> findAll() {
        return new ArrayList<User>(service.getAll());
    }

    @Override
    public PagingLoadResult<User> find(PagingLoadConfig pagingLoadConfig) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Result save(User user) {
        return service.save(user);
    }

    @Override
    public Result delete(User user) {
        return service.delete(user);
    }
}
