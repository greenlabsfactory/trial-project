package com.greenlabs.trialproject.web.server.service;

import com.greenlabs.trialproject.web.client.service.GwtAgamaService;
import com.greenlabs.trialproject.core.common.Result;
import com.greenlabs.trialproject.core.entity.Agama;
import com.greenlabs.trialproject.core.service.AgamaService;
import com.sencha.gxt.data.shared.loader.PagingLoadConfig;
import com.sencha.gxt.data.shared.loader.PagingLoadResult;
import com.sencha.gxt.data.shared.loader.PagingLoadResultBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

/**
 * Created by kris on 15/05/14.
 */
@Service("gwtAgamaService")
public class AgamaServiceImpl implements GwtAgamaService {

    @Autowired
    private AgamaService agamaService;

    @Override
    public Result save(Agama agama) {
        return agamaService.save(agama);
    }

    @Override
    public Result delete(Agama agama) {
        return agamaService.delete(agama);
    }

    @Override
    public PagingLoadResult<Agama> find(Agama agama, PagingLoadConfig pagingLoadConfig) {
        return new PagingLoadResultBean<>(agamaService.find(agama, pagingLoadConfig.getOffset(),
                pagingLoadConfig.getLimit()),
                agamaService.count(agama), pagingLoadConfig.getOffset()
        );
    }

    @Override
    public ArrayList<Agama> find(Agama agama) {
        return new ArrayList<>(agamaService.find(agama, 0, Integer.MAX_VALUE));
    }
}
