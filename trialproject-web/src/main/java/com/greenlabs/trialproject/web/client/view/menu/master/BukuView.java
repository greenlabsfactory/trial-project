package com.greenlabs.trialproject.web.client.view.menu.master;

import com.google.gwt.cell.client.DateCell;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Widget;
import com.greenlabs.trialproject.web.client.AppClient;
import com.greenlabs.trialproject.web.client.view.View;
import com.greenlabs.trialproject.web.client.view.custom.GridView;
import com.greenlabs.trialproject.web.client.view.custom.ToolbarGridView;
import com.greenlabs.trialproject.web.client.view.custom.ToolbarSearchType;
import com.greenlabs.trialproject.web.client.view.custom.WindowSearchView;
import com.greenlabs.trialproject.web.client.view.handler.GridHandler;
import com.greenlabs.trialproject.core.common.Pattern;
import com.greenlabs.trialproject.core.common.Result;
import com.greenlabs.trialproject.core.entity.Buku;
import com.greenlabs.trialproject.core.entity.Menu;
import com.sencha.gxt.data.client.loader.RpcProxy;
import com.sencha.gxt.data.shared.ListStore;
import com.sencha.gxt.data.shared.loader.LoadResultListStoreBinding;
import com.sencha.gxt.data.shared.loader.PagingLoadConfig;
import com.sencha.gxt.data.shared.loader.PagingLoadResult;
import com.sencha.gxt.data.shared.loader.PagingLoader;
import com.sencha.gxt.widget.core.client.box.ConfirmMessageBox;
import com.sencha.gxt.widget.core.client.container.VerticalLayoutContainer;
import com.sencha.gxt.widget.core.client.event.DialogHideEvent;
import com.sencha.gxt.widget.core.client.event.RowDoubleClickEvent;
import com.sencha.gxt.widget.core.client.event.SelectEvent;
import com.sencha.gxt.widget.core.client.grid.ColumnConfig;
import com.sencha.gxt.widget.core.client.grid.ColumnModel;
import com.sencha.gxt.widget.core.client.grid.Grid;
import com.sencha.gxt.widget.core.client.toolbar.PagingToolBar;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Tyas on 11/21/2014.
 */
public class BukuView extends View implements GridHandler {
    private Grid<Buku> grid;
    private PagingToolBar pagingToolBar;
    private PagingLoader<PagingLoadConfig, PagingLoadResult<Buku>> pagingLoader;
    private WindowSearchView windowSearchView;
    private Buku param;

    public BukuView(Menu menu) {
        super(menu);
    }

    @Override
    public Widget asWidget() {
        ListStore<Buku> listStore = new ListStore<>(getProperties().getBukuProperties().key());

        ColumnConfig<Buku, String> colKode = new ColumnConfig<>(getProperties().getBukuProperties().valueKode(), 100, "KODE");
        ColumnConfig<Buku, String> colJudul = new ColumnConfig<>(getProperties().getBukuProperties().valueJudul(), 100, "JUDUL");
        ColumnConfig<Buku, String> colPenulis = new ColumnConfig<>(getProperties().getBukuProperties().valuePenulis(), 100, "PENULIS");
        ColumnConfig<Buku, String> colPenerbit = new ColumnConfig<>(getProperties().getBukuProperties().valuePenerbit(), 100, "PENERBIT");
        ColumnConfig<Buku, String> colCreatedBy = new ColumnConfig<>(getProperties().getBukuProperties().valueCreatedBy(), 80, "CREATED BY");
        ColumnConfig<Buku, Date> colCreatedTime = new ColumnConfig<>(getProperties().getBukuProperties().valueCreatedTime(), 80, "CREATED TIME");
        colCreatedTime.setCell(new DateCell(DateTimeFormat.getFormat(Pattern.DATE_TIME_PATTERN)));

        ArrayList<ColumnConfig<Buku, ?>> list = new ArrayList<>();
        list.add(colKode);
        list.add(colJudul);
        list.add(colPenulis);
        list.add(colPenerbit);
        list.add(colCreatedBy);
        list.add(colCreatedTime);
        ColumnModel<Buku> columnModel = new ColumnModel<>(list);

        RpcProxy<PagingLoadConfig, PagingLoadResult<Buku>> proxy = new RpcProxy<PagingLoadConfig, PagingLoadResult<Buku>>() {
            @Override
            public void load(PagingLoadConfig loadConfig, AsyncCallback<PagingLoadResult<Buku>> callback) {
                param = new Buku();
                param.setKode(windowSearchView.getTextFieldKodeBuku().getCurrentValue());
                getService().getBukuServiceAsync().find(param, loadConfig, callback);
            }
        };

        pagingLoader = new PagingLoader<>(proxy);
        pagingLoader.addLoadHandler(new LoadResultListStoreBinding<PagingLoadConfig, Buku, PagingLoadResult<Buku>>(listStore));


        pagingToolBar = new PagingToolBar(AppClient.PAGING_SIZE);
        pagingToolBar.getElement().getStyle().setProperty("borderBottom", "none");
        pagingToolBar.bind(pagingLoader);

        grid = new Grid<Buku>(listStore, columnModel) {
            @Override
            protected void onAfterFirstAttach() {
                super.onAfterFirstAttach();
                Scheduler.get().scheduleDeferred(new Scheduler.ScheduledCommand() {
                    @Override
                    public void execute() {
                        pagingLoader.load();
                    }
                });
            }
        };

        grid.getView().setStripeRows(true);
        grid.getView().setColumnLines(true);
        grid.getView().setAutoFill(true);
        grid.setLoader(pagingLoader);
        grid.setLoadMask(true);
        grid.addRowDoubleClickHandler(gridRowDoubleClickHandler());

        windowSearchView = new WindowSearchView(ToolbarSearchType.BUKU) {
            @Override
            public SelectEvent.SelectHandler buttonSearchHandler() {
                return new SelectEvent.SelectHandler() {
                    @Override
                    public void onSelect(SelectEvent event) {
                        pagingToolBar.setActivePage(1);
                        pagingToolBar.refresh();
                        windowSearchView.hide();
                    }
                };
            }
        };

        GridView gridView = new GridView(getMenu());
        gridView.addWidget(createToolbar(), new VerticalLayoutContainer.VerticalLayoutData(1, -1));
        gridView.addWidget(grid, new VerticalLayoutContainer.VerticalLayoutData(1, 1));
        gridView.addWidget(pagingToolBar, new VerticalLayoutContainer.VerticalLayoutData(1, -1));
        return gridView;
    }

    private Widget createToolbar() {
        return new ToolbarGridView(super.getMenu()) {
            @Override
            public SelectEvent.SelectHandler buttonToolbarAddSelectHandler() {
                return buttonAddSelectHandler();
            }

            @Override
            public SelectEvent.SelectHandler buttonToolbarEditSelectHandler() {
                return buttonEditSelectHandler();
            }

            @Override
            public SelectEvent.SelectHandler buttonToolbarDeleteSelectHandler() {
                return buttonDeleteSelectHandler();
            }

            @Override
            public SelectEvent.SelectHandler buttonToolbarSearchSelectHandler() {
                return buttonSearchSelectHandler();
            }

            @Override
            public SelectEvent.SelectHandler buttonToolbarPrintSelectHandler() {
                return buttonPrintSelectHandler();
            }

        }.asWidget();

    }

    @Override
    public SelectEvent.SelectHandler buttonAddSelectHandler() {
        return new SelectEvent.SelectHandler() {
            @Override
            public void onSelect(SelectEvent event) {
                grid.getSelectionModel().deselectAll();
                new BukuFormView().showForm(BukuView.this);
            }
        };
    }

    @Override
    public SelectEvent.SelectHandler buttonEditSelectHandler() {
        return new SelectEvent.SelectHandler() {
            @Override
            public void onSelect(SelectEvent event) {
                if (grid.getSelectionModel().getSelectedItem() != null) {
                    new BukuFormView().showForm(BukuView.this);
                } else {
                    AppClient.showInfoMessage(Result.DATA_NOT_SELECTED);
                }
            }
        };
    }

    @Override
    public SelectEvent.SelectHandler buttonDeleteSelectHandler() {
        return new SelectEvent.SelectHandler() {
            @Override
            public void onSelect(SelectEvent event) {
                final Buku buku = grid.getSelectionModel().getSelectedItem();
                if (buku == null) {
                    AppClient.showInfoMessage(Result.DATA_NOT_SELECTED);
                    return;
                }

                final ConfirmMessageBox confirmMessageBox = AppClient.showMessageConfirmForDelete();
                confirmMessageBox.addDialogHideHandler(new DialogHideEvent.DialogHideHandler() {
                    @Override
                    public void onDialogHide(DialogHideEvent event) {
                        if (event.getHideButton().toString().equals(AppClient.MESSAGE_YES)) {
                            getService().getBukuServiceAsync().delete(buku, new AsyncCallback<Result>() {
                                @Override
                                public void onFailure(Throwable caught) {
                                    AppClient.showMessageOnFailureException(caught);
                                }

                                @Override
                                public void onSuccess(Result result) {
                                    AppClient.showInfoMessage(result.getMessage());
                                    if (result.getMessage().equals(Result.DELETE_SUCCESS)) {
                                        getPagingToolBar().refresh();
                                    }
                                }
                            });
                        } else {
                            grid.getSelectionModel().deselectAll();
                        }
                    }
                });
                confirmMessageBox.show();
            }
        };
    }

    @Override
    public SelectEvent.SelectHandler buttonSearchSelectHandler() {
        return new SelectEvent.SelectHandler() {
            @Override
            public void onSelect(SelectEvent event) {
                windowSearchView.show();
            }

        };
    }

    @Override
    public SelectEvent.SelectHandler buttonPrintSelectHandler() {
        return new SelectEvent.SelectHandler() {
            @Override
            public void onSelect(SelectEvent event) {

            }
        };
    }


    @Override
    public RowDoubleClickEvent.RowDoubleClickHandler gridRowDoubleClickHandler() {
        return new RowDoubleClickEvent.RowDoubleClickHandler() {
            @Override
            public void onRowDoubleClick(RowDoubleClickEvent event) {
                new BukuFormView().showForm(BukuView.this);
            }
        };
    }

    public PagingToolBar getPagingToolBar() {
        return pagingToolBar;
    }

    public Grid<Buku> getGrid() {
        return grid;
    }
}
