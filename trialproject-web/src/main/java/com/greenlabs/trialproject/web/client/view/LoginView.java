/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.greenlabs.trialproject.web.client.view;

import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyDownEvent;
import com.google.gwt.event.dom.client.KeyDownHandler;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.Widget;
import com.greenlabs.trialproject.web.client.AppClient;
import com.greenlabs.trialproject.web.client.wrapper.CallbackWrapper;
import com.greenlabs.trialproject.core.common.Result;
import com.greenlabs.trialproject.core.entity.User;
import com.sencha.gxt.widget.core.client.ContentPanel;
import com.sencha.gxt.widget.core.client.button.TextButton;
import com.sencha.gxt.widget.core.client.container.AbstractHtmlLayoutContainer.HtmlData;
import com.sencha.gxt.widget.core.client.container.HtmlLayoutContainer;
import com.sencha.gxt.widget.core.client.container.Viewport;
import com.sencha.gxt.widget.core.client.event.SelectEvent;
import com.sencha.gxt.widget.core.client.event.SelectEvent.SelectHandler;
import com.sencha.gxt.widget.core.client.form.PasswordField;
import com.sencha.gxt.widget.core.client.form.TextField;

/**
 * @author krissadewo
 */
public final class LoginView extends View {

    private PasswordField passwordField;
    private TextField textFieldUsername;
    private Timer sessionTimeoutResponseTimer;
    private int timeout = 0;

    @Override
    public Widget asWidget() {
        hideMessage();

        ContentPanel panel = new ContentPanel();
        panel.setHeaderVisible(false);
        panel.setBodyBorder(false);
        panel.setBodyStyleName("map");
        panel.setHeight("100%");

        HtmlLayoutContainer htmlLayoutContainer = new HtmlLayoutContainer(getLayout());
        panel.setWidget(htmlLayoutContainer);

        Label labelTitle = new Label("Greenlabs Medical Platform");
        labelTitle.setStyleName("app-title-login");
        htmlLayoutContainer.add(labelTitle, new HtmlData(".title"));

        textFieldUsername = new TextField();
        textFieldUsername.setWidth(300);
        textFieldUsername.setEmptyText("USERNAME");
//        textFieldUsername.setStyleName("textboxStyle");
        htmlLayoutContainer.add(textFieldUsername, new HtmlData(".username"));

        passwordField = new PasswordField();
        passwordField.setWidth(300);
        passwordField.setEmptyText("PASSWORD");
//        passwordField.setStyleName("textboxStyle");
        passwordField.addKeyDownHandler(new KeyDownHandler() {
            @Override
            public void onKeyDown(KeyDownEvent event) {
                if (event.getNativeEvent().getKeyCode() == KeyCodes.KEY_ENTER) {
                    doLogin();
                }
            }
        });
        htmlLayoutContainer.add(passwordField, new HtmlData(".password"));

        TextButton textButtonLogin = new TextButton();
        textButtonLogin.setText("Login");
        textButtonLogin.setWidth(100);
        textButtonLogin.setHeight(30);
//        textButtonLogin.setStyleName("button-login");
        htmlLayoutContainer.add(textButtonLogin, new HtmlData(".login"));
        textButtonLogin.addSelectHandler(addBtnLoginSelectHandler());

        Viewport viewport = new Viewport();
        viewport.add(panel);
        return viewport;
    }

    private native void hideMessage()/*-{
     $wnd.$(document).ready(function() {      
     setTimeout($wnd.$.unblockUI, 500);
     });
     }-*/;

    private native String getTableMarkup() /*-{
     return [ '<table style="width: 100%;padding-top:10%;padding-left:25%;padding-right:25%" class="map">',
     '<tr><td colspan="2" align="center"><div class=title></div></td></tr>',
     '<tr><td rowspan="2" style="width: 30%" class=logo></td></tr>',
     '<tr><td>',
     '<table style="background:white;">',
     '<tr><td><div class=labelUsername></div></td><td><div class=username></div></td></tr>',
     '<tr><td><div class=labelPassword></div></td><td><div class=password></div></td></tr>',
     '<tr><td colspan="2" class="login" align="right" style="width:275px;"></td></tr>',
     '</table>',
     '</td></tr>',     
     '</table>'
 
     ].join("");
     }-*/;

    private native String getLayout() /*-{
     return [ '<div style="width:100%;height:100%;">',
            '<div class="form">',
                '<div class="header-form">',
                '</div>',
                '<div class="body-form">',
                    '<table style="width:100%;" id="layout-login">',
                        '<tr class="title">',
                        '</tr>',
                        '<tr align="center">',
                            '<td>',
                                '<div class="username"></div>',
                            '</td>',
                        '</tr>',
                        '<tr align="center">',
                            '<td>',
                                '<div class="password"></div>',
                            '</td>',
                        '</tr>',
                        '<tr>',
                            '<td class="login" align="right">',
                            '</td>',
                        '</tr>',
                        '<tr>',
                            '<td align="center">',
                                '<label class="copyright">Copyright &copy; 2014 @ PT. Megagiga Solusindo</label>',
                            '</td>',
                        '</tr>',
                    '</table>',
                '</div>',
            '</div>',
        '</div>'
 
     ].join("");
     }-*/;

    private SelectHandler addBtnLoginSelectHandler() {

        return new SelectHandler() {
            @Override
            public void onSelect(SelectEvent event) {
                doLogin();
            }
        };
    }

    private void doLogin() {
        new CallbackWrapper<User>() {

            @Override
            protected void onSuccess(User user) {
                if (user != null) {
                    RootPanel.get().clear();
                    RootPanel.get().add(View.loadView(new HomeView()));
                    initSessionTimers();
                    return;
                }

                AppClient.showInfoMessage(Result.LOGIN_FAILED);
            }

            @Override
            protected void onFailure(Throwable throwable) {
            }

            @Override
            protected void onCall(AsyncCallback<User> callback) {
                getService().getUserServiceAsync().doLogin(new User(textFieldUsername.getText(), passwordField.getText()), callback);
            }
        }.call();
    }

    private void initSessionTimers() {
        getService().getSessionHandlerServiceAsync().getUserSessionTimeout(new AsyncCallback<Integer>() {
            @Override
            public void onFailure(Throwable caught) {
                AppClient.showMessageOnFailureException(caught);
            }

            @Override
            public void onSuccess(final Integer result) {
                timeout = result + 300000;
                sessionTimeoutResponseTimer = new Timer() {
                    @Override
                    public void run() {
                        isSessionAlive();
                    }
                };
                sessionTimeoutResponseTimer.schedule(timeout);
            }
        });
    }

    private void isSessionAlive() {
        getService().getSessionHandlerServiceAsync().isValidSession(new AsyncCallback<Boolean>() {
            @Override
            public void onFailure(Throwable caught) {
            }

            @Override
            public void onSuccess(Boolean valid) {
                if (valid) {
                    sessionTimeoutResponseTimer.cancel();
                    sessionTimeoutResponseTimer.scheduleRepeating(timeout);
                } else {
                    getService().getUserServiceAsync().doLogout(new AsyncCallback<Void>() {
                        @Override
                        public void onFailure(Throwable caught) {
                            AppClient.showMessageOnFailureException(caught);
                        }

                        @Override
                        public void onSuccess(Void result) {
                            Window.Location.reload();
                            sessionTimeoutResponseTimer.cancel();
                        }
                    });
                }
            }
        });
    }
}
