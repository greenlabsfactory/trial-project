/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.greenlabs.trialproject.web.shared;

/**
 * @author krissadewo
 */
public class UploadMessage {

    public static final String UPLOAD_SUCCESS = "success";
    public static final String UPLOAD_FAILED = "failed";
    private Integer successUpload;
    private Integer failedUpload;
    private String message;

    public UploadMessage() {
    }

    public UploadMessage(String message) {
        this.message = message;
    }

    public UploadMessage(Integer successUpload, Integer failedUpload) {
        this.successUpload = successUpload;
        this.failedUpload = failedUpload;
    }

    public Integer getSuccessUpload() {
        return successUpload;
    }

    public void setSuccessUpload(Integer successUpload) {
        this.successUpload = successUpload;
    }

    public Integer getFailedUpload() {
        return failedUpload;
    }

    public void setFailedUpload(Integer failedUpload) {
        this.failedUpload = failedUpload;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "UploadMessage{" + "successUpload=" + successUpload + ", failedUpload=" + failedUpload + '}';
    }

}
