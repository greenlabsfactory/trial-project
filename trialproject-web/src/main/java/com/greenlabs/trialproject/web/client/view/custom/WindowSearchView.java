/*
 * To change verticalLayoutContainer template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.greenlabs.trialproject.web.client.view.custom;

import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.greenlabs.trialproject.web.client.AppClient;
import com.greenlabs.trialproject.core.common.Pattern;
import com.greenlabs.trialproject.core.entity.Month;
import com.sencha.gxt.core.client.util.Margins;
import com.sencha.gxt.widget.core.client.FramedPanel;
import com.sencha.gxt.widget.core.client.Window;
import com.sencha.gxt.widget.core.client.button.TextButton;
import com.sencha.gxt.widget.core.client.container.HorizontalLayoutContainer;
import com.sencha.gxt.widget.core.client.container.MarginData;
import com.sencha.gxt.widget.core.client.container.VerticalLayoutContainer;
import com.sencha.gxt.widget.core.client.event.SelectEvent;
import com.sencha.gxt.widget.core.client.form.*;
import com.sencha.gxt.widget.core.client.toolbar.SeparatorToolItem;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author krissadewo
 */
public abstract class WindowSearchView extends Window {

    private FramedPanel framedPanel;
    private FormPanel formPanel;
    private VerticalLayoutContainer verticalLayoutContainer;
    private TextButton buttonSearch;
    private TextButton buttonReset;
    private DateField dateFieldTanggalAwal = new DateField(new DateTimePropertyEditor(Pattern.DD_MM_YYYY_WITH_STRIP));
    private DateField dateFieldTanggalAkhir = new DateField(new DateTimePropertyEditor(Pattern.DD_MM_YYYY_WITH_STRIP));
    private FieldYear fieldYear;
    private TextField textFieldAlamat = new TextField();
    private TextField textFieldNama = new TextField();
    private TextField textFieldKode = new TextField();
    private TextField textFieldNomor = new TextField();
    private TextField textFieldAlergi = new TextField();
    private TextField textFieldTelepon = new TextField();
    private TextField textFieldNoBatch = new TextField();
    private TextField textFieldKepemilikanObat = new TextField();
    private TextField textFieldKodeBuku = new TextField();
    private TextField textFieldNIM = new TextField();
    private TextField textFieldKodeBukuPeminjaman = new TextField();
    private ComboBox<Month> comboBoxMonth;

    private NumberField<BigDecimal> numberFieldHargaBeli;
    private NumberField<BigDecimal> numberFieldPersentase;
    private NumberField<BigDecimal> numberFieldHargaJual;

    public WindowSearchView(ToolbarSearchType toolbarSearchType) {
        super.setMinHeight(290);
        super.setHeight(290);
        super.setHeadingText("Pencarian");
        super.setWidth("500");
        super.setShadow(true);
        super.setModal(true);

        verticalLayoutContainer = new VerticalLayoutContainer();

        formPanel = new FormPanel();
        formPanel.add(verticalLayoutContainer, new MarginData(10, 10, 0, 10));

        framedPanel = new FramedPanel();
        framedPanel.setBorders(false);
        framedPanel.setHeaderVisible(false);
        framedPanel.add(formPanel);

        buttonSearch = new TextButtonSearch("Cari");
        buttonSearch.addSelectHandler(buttonSearchHandler());
        framedPanel.addButton(buttonSearch);

        buttonReset = new TextButtonReset();
        buttonReset.addSelectHandler(buttonRefreshResetHandler());
        framedPanel.addButton(buttonReset);

        dateFieldTanggalAwal.setValue(new Date());
        dateFieldTanggalAwal.setEmptyText(Pattern.DD_MM_YYYY_WITH_STRIP);
        dateFieldTanggalAwal.setEditable(false);
        dateFieldTanggalAwal.getDatePicker().addValueChangeHandler(new ValueChangeHandler<Date>() {
            @Override
            public void onValueChange(ValueChangeEvent<Date> event) {
                dateFieldTanggalAwal.setValue(event.getValue());
            }
        });

        dateFieldTanggalAkhir.setValue(new Date());
        dateFieldTanggalAkhir.setEmptyText(Pattern.DD_MM_YYYY_WITH_STRIP);
        dateFieldTanggalAkhir.setEditable(false);
        dateFieldTanggalAkhir.getDatePicker().addValueChangeHandler(new ValueChangeHandler<Date>() {
            @Override
            public void onValueChange(ValueChangeEvent<Date> event) {
                dateFieldTanggalAkhir.setValue(event.getValue());
            }
        });

        numberFieldHargaBeli = new NumberField<>(new NumberPropertyEditor.BigDecimalPropertyEditor(AppClient.CURRENCY_FORMAT));
        numberFieldHargaBeli.setValue(new BigDecimal(0));

        numberFieldPersentase = new NumberField<>(new NumberPropertyEditor.BigDecimalPropertyEditor(AppClient.CURRENCY_FORMAT));
        numberFieldPersentase.setValue(new BigDecimal(0));

        numberFieldHargaJual = new NumberField<>(new NumberPropertyEditor.BigDecimalPropertyEditor(AppClient.CURRENCY_FORMAT));
        numberFieldHargaJual.setValue(new BigDecimal(0));

        final HorizontalLayoutContainer horizontalPeriodeDate = new HorizontalLayoutContainer();
        horizontalPeriodeDate.setLayoutData(new HorizontalLayoutContainer.HorizontalLayoutData(1, -1, new Margins(0, 4, 0, 0)));
        horizontalPeriodeDate.add(dateFieldTanggalAwal);
        horizontalPeriodeDate.add(new SeparatorToolItem());
        horizontalPeriodeDate.add(dateFieldTanggalAkhir);

        fieldYear = new FieldYear(new NumberPropertyEditor.IntegerPropertyEditor());
        comboBoxMonth = (ComboBox<Month>) new ComboBoxMonth().asWidget();
        comboBoxMonth.setWidth(120);

        final HorizontalLayoutContainer horizontalPeriodeMonth = new HorizontalLayoutContainer();
        horizontalPeriodeMonth.setLayoutData(new HorizontalLayoutContainer.HorizontalLayoutData(1, -1, new Margins(0, 4, 0, 0)));
        horizontalPeriodeMonth.add(comboBoxMonth);
        horizontalPeriodeMonth.add(new SeparatorToolItem());
        horizontalPeriodeMonth.add(fieldYear);

        if (toolbarSearchType == ToolbarSearchType.PROPINSI) {
            verticalLayoutContainer.add(new CustomFieldLabel(textFieldNama, "Nama Propinsi"), new VerticalLayoutContainer.VerticalLayoutData(1, -1));

            super.setHeight(140);
            super.setMinHeight(140);
        } else if (toolbarSearchType == ToolbarSearchType.KABUPATEN) {
            verticalLayoutContainer.add(new CustomFieldLabel(textFieldNama, "Nama Kabupaten"), new VerticalLayoutContainer.VerticalLayoutData(1, -1));

            super.setHeight(140);
            super.setMinHeight(140);
        } else if (toolbarSearchType == ToolbarSearchType.KECAMATAN) {
            verticalLayoutContainer.add(new CustomFieldLabel(textFieldNama, "Nama Kecamatan"), new VerticalLayoutContainer.VerticalLayoutData(1, -1));

            super.setHeight(140);
            super.setMinHeight(140);
        } else if (toolbarSearchType == ToolbarSearchType.KELURAHAN) {
            verticalLayoutContainer.add(new CustomFieldLabel(textFieldNama, "Nama Kelurahan"), new VerticalLayoutContainer.VerticalLayoutData(1, -1));

            super.setHeight(140);
            super.setMinHeight(140);
        } else if (toolbarSearchType == ToolbarSearchType.AGAMA) {
            verticalLayoutContainer.add(new CustomFieldLabel(textFieldNama, "Nama Agama"), new VerticalLayoutContainer.VerticalLayoutData(1, -1));

            super.setHeight(140);
            super.setMinHeight(140);
        }  else if (toolbarSearchType == ToolbarSearchType.BUKU){
            verticalLayoutContainer.add(new CustomFieldLabel(textFieldKodeBuku, "Kode Buku"), new VerticalLayoutContainer.VerticalLayoutData(1, -1));

            super.setHeight(140);
            super.setMinHeight(140);
        }  else if (toolbarSearchType == ToolbarSearchType.MAHASISWA){
            verticalLayoutContainer.add(new CustomFieldLabel(textFieldNIM, "NIM"), new VerticalLayoutContainer.VerticalLayoutData(1, -1));

            super.setHeight(140);
            super.setMinHeight(140);
        } else if (toolbarSearchType == ToolbarSearchType.PEMINJAMAN){
            verticalLayoutContainer.add(new CustomFieldLabel(textFieldKodeBukuPeminjaman, "Kode Buku"), new VerticalLayoutContainer.VerticalLayoutData(1, -1));

            super.setHeight(140);
            super.setMinHeight(140);
        }else if (toolbarSearchType == ToolbarSearchType.ASURANSI) {
            verticalLayoutContainer.add(new CustomFieldLabel(textFieldNama, "Nama Asuransi"), new VerticalLayoutContainer.VerticalLayoutData(1, -1));
            verticalLayoutContainer.add(new CustomFieldLabel(textFieldAlamat, "Alamat"), new VerticalLayoutContainer.VerticalLayoutData(1, -1));

            super.setHeight(180);
            super.setMinHeight(180);
        } else if (toolbarSearchType == ToolbarSearchType.BANGSAL) {
            verticalLayoutContainer.add(new CustomFieldLabel(textFieldNama, "Nama Bangsal"), new VerticalLayoutContainer.VerticalLayoutData(1, -1));

            super.setHeight(140);
            super.setMinHeight(140);
        } else if (toolbarSearchType == ToolbarSearchType.CARA_MASUK) {
            verticalLayoutContainer.add(new CustomFieldLabel(textFieldNama, "Keterangan Cara Masuk"), new VerticalLayoutContainer.VerticalLayoutData(1, -1));

            super.setHeight(140);
            super.setMinHeight(140);
        } else if (toolbarSearchType == ToolbarSearchType.CARA_BAYAR) {
            verticalLayoutContainer.add(new CustomFieldLabel(textFieldNama, "Keterangan"), new VerticalLayoutContainer.VerticalLayoutData(1, -1));

            super.setHeight(140);
            super.setMinHeight(140);
        } else if (toolbarSearchType == ToolbarSearchType.PASIEN_JENIS) {
            verticalLayoutContainer.add(new CustomFieldLabel(textFieldNama, "Jenis Pasien"), new VerticalLayoutContainer.VerticalLayoutData(1, -1));

            super.setHeight(140);
            super.setMinHeight(140);
        } else if (toolbarSearchType == ToolbarSearchType.KELAS) {
            verticalLayoutContainer.add(new CustomFieldLabel(textFieldNama, "Kelas Kamar"), new VerticalLayoutContainer.VerticalLayoutData(1, -1));

            super.setHeight(140);
            super.setMinHeight(140);
        } else if (toolbarSearchType == ToolbarSearchType.PASIEN) {
            verticalLayoutContainer.add(new CustomFieldLabel(textFieldNama, "Nama Pasien"), new VerticalLayoutContainer.VerticalLayoutData(1, -1));
            verticalLayoutContainer.add(new CustomFieldLabel(textFieldAlamat, "Alamat Pasien"), new VerticalLayoutContainer.VerticalLayoutData(1, -1));
            verticalLayoutContainer.add(new CustomFieldLabel(textFieldNomor, "Nomor ID Pasien"), new VerticalLayoutContainer.VerticalLayoutData(1, -1));
            verticalLayoutContainer.add(new CustomFieldLabel(textFieldAlergi, "Alergi"), new VerticalLayoutContainer.VerticalLayoutData(1, -1));
            verticalLayoutContainer.add(new CustomFieldLabel(textFieldTelepon, "Telepon"), new VerticalLayoutContainer.VerticalLayoutData(1, -1));

            super.setHeight(340);
            super.setMinHeight(340);
        } else if (toolbarSearchType == ToolbarSearchType.PENDIDIKAN) {
            verticalLayoutContainer.add(new CustomFieldLabel(textFieldNama, "Nama Pendidikan"), new VerticalLayoutContainer.VerticalLayoutData(1, -1));

            super.setHeight(140);
            super.setMinHeight(140);
        } else if (toolbarSearchType == ToolbarSearchType.PEKERJAAN) {
            verticalLayoutContainer.add(new CustomFieldLabel(textFieldNama, "Nama Pekerjaan"), new VerticalLayoutContainer.VerticalLayoutData(1, -1));

            super.setHeight(140);
            super.setMinHeight(140);
        } else if (toolbarSearchType == ToolbarSearchType.OBAT) {
            verticalLayoutContainer.add(new CustomFieldLabel(textFieldNama, "Nama Obat"), new VerticalLayoutContainer.VerticalLayoutData(1, -1));

            super.setHeight(140);
            super.setMinHeight(140);
        } else if (toolbarSearchType == ToolbarSearchType.GOLONGAN_OBAT) {
            verticalLayoutContainer.add(new CustomFieldLabel(textFieldNama, "Nama Golongan Obat"), new VerticalLayoutContainer.VerticalLayoutData(1, -1));

            super.setHeight(140);
            super.setMinHeight(140);
        } else if (toolbarSearchType == ToolbarSearchType.JENIS_OBAT) {
            verticalLayoutContainer.add(new CustomFieldLabel(textFieldNama, "Nama Jenis Obat"), new VerticalLayoutContainer.VerticalLayoutData(1, -1));

            super.setHeight(140);
            super.setMinHeight(140);
        } else if (toolbarSearchType == ToolbarSearchType.KEPEMILIKAN_OBAT) {
            verticalLayoutContainer.add(new CustomFieldLabel(textFieldNama, "Nama Pemilik"), new VerticalLayoutContainer.VerticalLayoutData(1, -1));

            super.setHeight(140);
            super.setMinHeight(140);
        } else if (toolbarSearchType == ToolbarSearchType.SATUAN_OBAT_BESAR) {
            verticalLayoutContainer.add(new CustomFieldLabel(textFieldNama, "Nama Satuan Obat Besar"), new VerticalLayoutContainer.VerticalLayoutData(1, -1));

            super.setHeight(140);
            super.setMinHeight(140);
        } else if (toolbarSearchType == ToolbarSearchType.SATUAN_OBAT_KECIL) {
            verticalLayoutContainer.add(new CustomFieldLabel(textFieldNama, "Nama Satuan Obat Kecil"), new VerticalLayoutContainer.VerticalLayoutData(1, -1));

            super.setHeight(140);
            super.setMinHeight(140);
        } else if (toolbarSearchType == ToolbarSearchType.SEDIAAN_OBAT) {
            verticalLayoutContainer.add(new CustomFieldLabel(textFieldNama, "Nama Sediaan Obat"), new VerticalLayoutContainer.VerticalLayoutData(1, -1));

            super.setHeight(140);
            super.setMinHeight(140);
        } else if (toolbarSearchType == ToolbarSearchType.TERAPI_OBAT) {
            verticalLayoutContainer.add(new CustomFieldLabel(textFieldNama, "Nama TerapiObat Obat"), new VerticalLayoutContainer.VerticalLayoutData(1, -1));

            super.setHeight(140);
            super.setMinHeight(140);
        } else if (toolbarSearchType == ToolbarSearchType.DOKTER) {
            verticalLayoutContainer.add(new CustomFieldLabel(textFieldNama, "Nama Dokter"), new VerticalLayoutContainer.VerticalLayoutData(1, -1));

            super.setHeight(140);
            super.setMinHeight(140);
        } else if (toolbarSearchType == ToolbarSearchType.RUJUKAN) {
            verticalLayoutContainer.add(new CustomFieldLabel(textFieldNama, "Nama"), new VerticalLayoutContainer.VerticalLayoutData(1, -1));
            verticalLayoutContainer.add(new CustomFieldLabel(textFieldAlamat, "Alamat"), new VerticalLayoutContainer.VerticalLayoutData(1, -1));

            super.setHeight(200);
            super.setMinHeight(200);
        } else if (toolbarSearchType == ToolbarSearchType.DIAGNOSA) {
            verticalLayoutContainer.add(new CustomFieldLabel(textFieldNama, "Nama Diagnosa"), new VerticalLayoutContainer.VerticalLayoutData(1, -1));

            super.setHeight(140);
            super.setMinHeight(140);
        } else if (toolbarSearchType == ToolbarSearchType.TINDAKAN) {
            verticalLayoutContainer.add(new CustomFieldLabel(textFieldKode, "Kode"), new VerticalLayoutContainer.VerticalLayoutData(1, -1));
            verticalLayoutContainer.add(new CustomFieldLabel(textFieldNama, "Nama"), new VerticalLayoutContainer.VerticalLayoutData(1, -1));

            super.setHeight(160);
            super.setMinHeight(160);
        } else if (toolbarSearchType == ToolbarSearchType.PRODUK) {
            verticalLayoutContainer.add(new CustomFieldLabel(textFieldNama, "Nama Produk"), new VerticalLayoutContainer.VerticalLayoutData(1, -1));

            super.setHeadingText("Pencarian Produk");
            super.setHeight(140);
            super.setMinHeight(140);
        } else if (toolbarSearchType == ToolbarSearchType.PRODUK_GOLONGAN) {
            verticalLayoutContainer.add(new CustomFieldLabel(textFieldNama, "Nama Golongan Produk", 140), new VerticalLayoutContainer.VerticalLayoutData(1, -1));

            super.setHeadingText("Pencarian Golongan Produk");
            super.setHeight(140);
            super.setMinHeight(140);
        } else if (toolbarSearchType == ToolbarSearchType.POLIKLINIK) {
            verticalLayoutContainer.add(new CustomFieldLabel(textFieldNama, "Nama Poliklinik"), new VerticalLayoutContainer.VerticalLayoutData(1, -1));

            super.setHeadingText("Pencarian Poliklinik");
            super.setHeight(140);
            super.setMinHeight(140);
        } else if (toolbarSearchType == ToolbarSearchType.SPESIALISASI) {
            verticalLayoutContainer.add(new CustomFieldLabel(textFieldNama, "Nama Spesialisasi"), new VerticalLayoutContainer.VerticalLayoutData(1, -1));

            super.setHeadingText("Pencarian Spesialisasi");
            super.setHeight(140);
            super.setMinHeight(140);
        } else if (toolbarSearchType == ToolbarSearchType.SUB_SPESIALISASI) {
            verticalLayoutContainer.add(new CustomFieldLabel(textFieldNama, "Nama Sub Spesialisasi", 150), new VerticalLayoutContainer.VerticalLayoutData(1, -1));

            super.setHeadingText("Pencarian Sub Spesialisasi");
            super.setHeight(140);
            super.setMinHeight(140);
        } else if (toolbarSearchType == ToolbarSearchType.JENIS_VENDOR) {
            verticalLayoutContainer.add(new CustomFieldLabel(textFieldNama, "Nama", 150), new VerticalLayoutContainer.VerticalLayoutData(1, -1));

            super.setHeadingText("Pencarian Jenis Vendor");
            super.setHeight(140);
            super.setMinHeight(140);
        } else if (toolbarSearchType == ToolbarSearchType.KOMPONEN_BIAYA) {
            verticalLayoutContainer.add(new CustomFieldLabel(textFieldNama, "Nama Komponen"), new VerticalLayoutContainer.VerticalLayoutData(1, -1));

            super.setHeight(150);
            super.setMinHeight(150);
        } else if (toolbarSearchType == ToolbarSearchType.UNIT) {
            textFieldNama = new TextField();
            textFieldKode = new TextField();

            verticalLayoutContainer.add(new CustomFieldLabel(textFieldKode, "Kode"), new VerticalLayoutContainer.VerticalLayoutData(1, -1));
            verticalLayoutContainer.add(new CustomFieldLabel(textFieldNama, "Nama"), new VerticalLayoutContainer.VerticalLayoutData(1, -1));

            super.setHeight(180);
            super.setMinHeight(180);
        }

        super.setResizable(false);
        super.add(framedPanel, new MarginData(5, 3, 1, 3));
    }


    private SelectEvent.SelectHandler buttonRefreshResetHandler() {
        return new SelectEvent.SelectHandler() {
            @Override
            public void onSelect(SelectEvent event) {
                reset();
            }
        };
    }

    public abstract SelectEvent.SelectHandler buttonSearchHandler();

    public void reset() {
        formPanel.reset();
        dateFieldTanggalAwal.setValue(new Date());
        dateFieldTanggalAkhir.setValue(new Date());

    }

    public DateField getDateFieldTanggalAwal() {
        return dateFieldTanggalAwal;
    }

    public DateField getDateFieldTanggalAkhir() {
        return dateFieldTanggalAkhir;
    }

    public FieldYear getFieldYear() {
        return fieldYear;
    }

    public TextField getTextFieldAlamat() {
        return textFieldAlamat;
    }

    public TextField getTextFieldNama() {
        return textFieldNama;
    }

    public TextField getTextFieldKode() {
        return textFieldKode;
    }

    public TextField getTextFieldKodeBuku() {
        return textFieldKodeBuku;
    }

    public TextField getTextFieldNIM() {
        return textFieldNIM;
    }

    public TextField getTextFieldNomor() {
        return textFieldNomor;
    }

    public TextField getTextFieldAlergi() {
        return textFieldAlergi;
    }

    public TextField getTextFieldTelepon() {
        return textFieldTelepon;
    }

    public TextField getTextFieldNoBatch() {
        return textFieldNoBatch;
    }

    public TextField getTextFieldKepemilikanObat() {
        return textFieldKepemilikanObat;
    }

    public TextField getTextFieldKodeBukuPeminjaman() {
        return textFieldKodeBukuPeminjaman;
    }




    public NumberField<BigDecimal> getNumberFieldHargaBeli() {
        return numberFieldHargaBeli;
    }

    public NumberField<BigDecimal> getNumberFieldPersentase() {
        return numberFieldPersentase;
    }

    public NumberField<BigDecimal> getNumberFieldHargaJual() {
        return numberFieldHargaJual;
    }


}
