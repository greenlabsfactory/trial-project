/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.greenlabs.trialproject.web.client.service;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.greenlabs.trialproject.core.entity.RoleUser;
import com.greenlabs.trialproject.core.entity.User;

import java.util.ArrayList;

/**
 * @author krissadewo
 */
public interface GwtRoleUserServiceAsync {

    void find(User user, AsyncCallback<ArrayList<RoleUser>> callback);
}
