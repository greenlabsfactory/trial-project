/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.greenlabs.trialproject.web.client.service;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.greenlabs.trialproject.core.entity.Role;

import java.util.ArrayList;

/**
 * @author krissadewo <dailycode.org>
 * @date Jul 25, 2013
 */
@RemoteServiceRelativePath("springGwtServices/gwtRoleService")
public interface GwtRoleService extends RemoteService {

    ArrayList<Role> findAll();
}
