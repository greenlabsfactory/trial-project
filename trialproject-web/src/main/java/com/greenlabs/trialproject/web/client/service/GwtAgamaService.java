/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.greenlabs.trialproject.web.client.service;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.greenlabs.trialproject.core.common.Result;
import com.greenlabs.trialproject.core.entity.Agama;
import com.sencha.gxt.data.shared.loader.PagingLoadConfig;
import com.sencha.gxt.data.shared.loader.PagingLoadResult;

import java.util.ArrayList;

/**
 * @author krissadewo
 */
@RemoteServiceRelativePath("springGwtServices/gwtAgamaService")
public interface GwtAgamaService extends RemoteService {

    Result save(Agama agama);

    Result delete(Agama agama);

    PagingLoadResult<Agama> find(Agama agama, PagingLoadConfig pagingLoadConfig);

    ArrayList<Agama> find(Agama agama);
}
