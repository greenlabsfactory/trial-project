/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.greenlabs.trialproject.web.client.view.custom;

import com.greenlabs.trialproject.web.client.icon.Icon;
import com.sencha.gxt.cell.core.client.ButtonCell;
import com.sencha.gxt.widget.core.client.button.TextButton;

/**
 * @author krissadewo <dailycode.org>
 * @date Jul 15, 2013
 */
public class TextButtonReset extends TextButton {

    public TextButtonReset() {
        super("Reset");
        this.setIcon(Icon.INSTANCE.reset());
        this.setIconAlign(ButtonCell.IconAlign.RIGHT);
    }
}
