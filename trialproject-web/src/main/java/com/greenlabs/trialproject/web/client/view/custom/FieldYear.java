/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.greenlabs.trialproject.web.client.view.custom;

import com.sencha.gxt.widget.core.client.form.NumberPropertyEditor;
import com.sencha.gxt.widget.core.client.form.SpinnerField;

import java.util.Date;

/**
 * @author krissadewo
 */
public class FieldYear extends SpinnerField<Integer> {

    public FieldYear(NumberPropertyEditor<Integer> editor) {
        super(editor);
        setValue(new Date().getYear() + 1900);
        setOriginalValue(getValue());
        setIncrement(1);
        setMinValue(2005);
        setAllowNegative(false);
        setAllowBlank(false);
        setWidth(60);
    }
}
