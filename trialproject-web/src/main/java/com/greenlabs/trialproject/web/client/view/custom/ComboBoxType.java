/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.greenlabs.trialproject.web.client.view.custom;

/**
 * @author krissadewo
 */
public enum ComboBoxType {

    MONTH,
    JENIS_KABUPATEN,
    JENIS_IDENTITAS,
    JENIS_KELAMIN,
    GOLONGAN_DARAH,
    GENERIK_OBAT,
    HARI,
    STATUS_NIKAH,
    STATUS_PELAYANAN,
    STATUS_BAYAR,
    WARGA_NEGARA,
    HUBUNGAN_KELUARGA,
    METODE_HITUNG,
    KELANJUTAN_KUNJUNGAN_JALAN,
    KELANJUTAN_KUNJUNGAN_INAP,
}
