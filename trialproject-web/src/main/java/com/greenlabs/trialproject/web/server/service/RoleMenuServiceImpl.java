/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.greenlabs.trialproject.web.server.service;

import com.greenlabs.trialproject.web.client.service.GwtRoleMenuService;
import com.greenlabs.trialproject.core.common.Result;
import com.greenlabs.trialproject.core.entity.RoleMenu;
import com.greenlabs.trialproject.core.service.RoleMenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author krissadewo <dailycode.org>
 * @date Jul 25, 2013
 */
@Service("gwtRoleMenuService")
public class RoleMenuServiceImpl implements GwtRoleMenuService {

    @Autowired
    private RoleMenuService service;

    @Override
    public Result save(List<RoleMenu> roleMenus) {
        return service.save(roleMenus);
    }
}
