/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.greenlabs.trialproject.web.client.view.custom;

import com.greenlabs.trialproject.web.client.icon.Icon;
import com.sencha.gxt.cell.core.client.ButtonCell;
import com.sencha.gxt.widget.core.client.button.TextButton;

/**
 * @author krissadewo <dailycode.org>
 * @date Jul 15, 2013
 */
public class TextButtonSave extends TextButton {

    public TextButtonSave() {
        super("Simpan");
        this.setIcon(Icon.INSTANCE.save());
        this.setIconAlign(ButtonCell.IconAlign.RIGHT);
    }
}
