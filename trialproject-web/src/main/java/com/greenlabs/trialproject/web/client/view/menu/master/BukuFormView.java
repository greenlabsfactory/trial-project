package com.greenlabs.trialproject.web.client.view.menu.master;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.greenlabs.trialproject.web.client.AppClient;
import com.greenlabs.trialproject.web.client.view.View;
import com.greenlabs.trialproject.web.client.view.custom.CustomFieldLabel;
import com.greenlabs.trialproject.web.client.view.custom.WindowFormView;
import com.greenlabs.trialproject.web.client.view.handler.FormHandler;
import com.greenlabs.trialproject.core.common.Result;
import com.greenlabs.trialproject.core.entity.Buku;
import com.sencha.gxt.widget.core.client.container.VerticalLayoutContainer;
import com.sencha.gxt.widget.core.client.event.SelectEvent;
import com.sencha.gxt.widget.core.client.form.TextField;

/**
 * Created by Tyas on 11/21/2014.
 */
public class BukuFormView extends View implements FormHandler {
    private WindowFormView windowFormView ;
    private BukuView parentView;
    private TextField textFieldKode;
    private TextField textFieldJudul;
    private TextField textFieldPenulis;
    private TextField textFieldPenerbit;
    private Buku buku;
    @Override
    public void showForm(View view) {
        parentView = (BukuView) view;
        textFieldKode = new TextField();
        textFieldJudul = new TextField();
        textFieldPenulis = new TextField();
        textFieldPenerbit= new TextField();
        textFieldKode.setAllowBlank(false);
        textFieldJudul.setAllowBlank(false);
        textFieldPenulis.setAllowBlank(false);
        textFieldPenerbit.setAllowBlank(false);

        if (parentView.getGrid().getSelectionModel().getSelectedItem() != null) {
            buku = parentView.getGrid().getSelectionModel().getSelectedItem();
            textFieldKode.setValue(buku.getKode());
            textFieldJudul.setValue(buku.getJudul());
            textFieldPenulis.setValue(buku.getPenulis());
            textFieldPenerbit.setValue(buku.getPenerbit());
        } else {
            buku = new Buku();
        }

        windowFormView = new WindowFormView(parentView.getGrid(), parentView.getMenu());
        windowFormView.addWidget(new CustomFieldLabel(textFieldKode, "Kode"), new VerticalLayoutContainer.VerticalLayoutData(1, -1));
        windowFormView.addWidget(new CustomFieldLabel(textFieldJudul, "Judul"), new VerticalLayoutContainer.VerticalLayoutData(1, -1));
        windowFormView.addWidget(new CustomFieldLabel(textFieldPenulis, "Penulis"), new VerticalLayoutContainer.VerticalLayoutData(1, -1));
        windowFormView.addWidget(new CustomFieldLabel(textFieldPenerbit, "Penerbit"), new VerticalLayoutContainer.VerticalLayoutData(1, -1));
        windowFormView.getButtonSave().addSelectHandler(buttonSaveSelectHandler());
        windowFormView.setCursorPosition(textFieldKode);

        windowFormView.show();
    }

    @Override
    public SelectEvent.SelectHandler buttonSaveSelectHandler() {
        return new SelectEvent.SelectHandler() {
            @Override
            public void onSelect(SelectEvent event) {
                if (!windowFormView.getFormPanel().isValid()) {
                    return;
                }
                buku.setKode(textFieldKode.getCurrentValue());
                buku.setJudul(textFieldJudul.getCurrentValue());
                buku.setPenulis(textFieldPenulis.getCurrentValue());
                buku.setPenerbit(textFieldPenerbit.getCurrentValue());

                getService().getBukuServiceAsync().save(buku, new AsyncCallback<Result>() {
                    @Override
                    public void onFailure(Throwable throwable) {

                    }

                    @Override
                    public void onSuccess(Result result) {
                        if (result.getMessage().equals(Result.SAVE_SUCCESS)) {
                            windowFormView.hide();
                        } else {
                            windowFormView.getButtonSave().setEnabled(true);
                        }
                        AppClient.showInfoMessage(result.getMessage(), parentView.getPagingToolBar());
                    }
                });

            }
        };
    }
}
