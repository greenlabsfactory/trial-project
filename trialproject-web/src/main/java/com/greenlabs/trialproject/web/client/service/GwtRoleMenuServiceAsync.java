/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.greenlabs.trialproject.web.client.service;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.greenlabs.trialproject.core.common.Result;
import com.greenlabs.trialproject.core.entity.RoleMenu;

import java.util.List;

/**
 * @author krissadewo <dailycode.org>
 * @date Jul 25, 2013
 */
public interface GwtRoleMenuServiceAsync {

    void save(List<RoleMenu> roleMenus, AsyncCallback<Result> callback);
}
