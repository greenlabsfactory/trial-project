/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.greenlabs.trialproject.web.client.view.custom;

import com.google.gwt.user.client.ui.Widget;
import com.sencha.gxt.widget.core.client.FramedPanel;
import com.sencha.gxt.widget.core.client.button.TextButton;
import com.sencha.gxt.widget.core.client.container.VerticalLayoutContainer;
import com.sencha.gxt.widget.core.client.event.HideEvent;
import com.sencha.gxt.widget.core.client.form.FormPanel;

/**
 * @author krissadewo
 *         Provide standard form input with save and cancel functionality
 */
public class FormView extends FramedPanel {

    private FormPanel formPanel;
    private VerticalLayoutContainer verticalLayoutContainer;
    private TextButton buttonSave;
    private TextButton buttonCancel;
    private boolean reset;

    public FormView() {
        verticalLayoutContainer = new VerticalLayoutContainer();
        formPanel = new FormPanel();
        formPanel.add(verticalLayoutContainer);

        super.setHeaderVisible(false);
        super.add(formPanel);

        buttonSave = new TextButtonSave();
        buttonCancel = new TextButtonReset();

        super.addButton(buttonSave);
        super.addButton(buttonCancel);
        super.addHideHandler(new HideEvent.HideHandler() {
            @Override
            public void onHide(HideEvent event) {
                if (isReset()) {
                    formPanel.reset();
                }
            }
        });
    }

    /**
     * Add widget to the vertical layout container
     *
     * @param widget             the widget
     * @param verticalLayoutData layout
     */
    public void addWidget(Widget widget, VerticalLayoutContainer.VerticalLayoutData verticalLayoutData) {
        verticalLayoutContainer.add(widget, verticalLayoutData);
    }

    public TextButton getButtonSave() {
        return buttonSave;
    }

    public TextButton getButtonCancel() {
        return buttonCancel;
    }

    public FormPanel getFormPanel() {
        return formPanel;
    }

    public boolean isReset() {
        return reset;
    }

    public void setReset(boolean reset) {
        this.reset = reset;
    }
}
