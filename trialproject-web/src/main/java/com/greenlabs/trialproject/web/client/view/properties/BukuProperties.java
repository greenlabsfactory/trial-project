package com.greenlabs.trialproject.web.client.view.properties;

import com.google.gwt.editor.client.Editor;
import com.greenlabs.trialproject.core.entity.Agama;
import com.greenlabs.trialproject.core.entity.Buku;
import com.sencha.gxt.core.client.ValueProvider;
import com.sencha.gxt.data.shared.LabelProvider;
import com.sencha.gxt.data.shared.ModelKeyProvider;
import com.sencha.gxt.data.shared.PropertyAccess;

import java.util.Date;

/**
 * Created by Tyas on 11/21/2014.
 */
public interface BukuProperties extends PropertyAccess<Buku> {

    @Editor.Path("id")
    ModelKeyProvider<Buku> key();

    @Editor.Path("kode")
    LabelProvider<Buku> labelKode();

    @Editor.Path("kode")
    ValueProvider<Buku, String> valueKode();

    @Editor.Path("judul")
    LabelProvider<Buku> labelJudul();

    @Editor.Path("judul")
    ValueProvider<Buku, String> valueJudul();

    @Editor.Path("penulis")
    LabelProvider<Buku> labelPenulis();

    @Editor.Path("penulis")
    ValueProvider<Buku, String> valuePenulis();

    @Editor.Path("penerbit")
    LabelProvider<Buku> labelPenerbit();

    @Editor.Path("penerbit")
    ValueProvider<Buku, String> valuePenerbit();

    @Editor.Path("createdTime")
    ValueProvider<Buku, Date> valueCreatedTime();

    @Editor.Path("createdBy.realname")
    ValueProvider<Buku, String> valueCreatedBy();
}
