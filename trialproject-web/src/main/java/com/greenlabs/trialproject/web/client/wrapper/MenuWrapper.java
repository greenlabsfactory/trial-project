/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.greenlabs.trialproject.web.client.wrapper;

import com.google.gwt.user.client.ui.Widget;
import com.greenlabs.trialproject.core.entity.Menu;

/**
 * @author kris
 */
public class MenuWrapper {

    private Menu menu;
    private Widget widget;

    public MenuWrapper() {
    }

    public MenuWrapper(Menu menu, Widget widget) {
        this.menu = menu;
        this.widget = widget;
    }

    public Menu getMenu() {
        return menu;
    }

    public void setMenu(Menu menu) {
        this.menu = menu;
    }

    public Widget getWidget() {
        return widget;
    }

    public void setWidget(Widget widget) {
        this.widget = widget;
    }

    @Override
    public String toString() {
        return "HomeMenuWrapper{" + "menu=" + menu + ", widget=" + widget + '}';
    }
}
