package com.greenlabs.trialproject.web.client.service;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.greenlabs.trialproject.core.common.Result;
import com.greenlabs.trialproject.core.entity.Agama;
import com.greenlabs.trialproject.core.entity.Buku;
import com.sencha.gxt.data.shared.loader.PagingLoadConfig;
import com.sencha.gxt.data.shared.loader.PagingLoadResult;

import java.util.ArrayList;

/**
 * Created by Tyas on 11/21/2014.
 */
@RemoteServiceRelativePath("springGwtServices/gwtBukuService")
public interface GwtBukuService extends RemoteService {
    Result save(Buku buku);

    Result delete(Buku buku);

    PagingLoadResult<Buku> find(Buku buku, PagingLoadConfig pagingLoadConfig);

    ArrayList<Buku> find(Buku buku);
}
